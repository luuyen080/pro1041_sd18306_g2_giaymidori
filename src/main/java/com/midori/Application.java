package com.midori;

import com.formdev.flatlaf.FlatLightLaf;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import com.midori.configs.JdbcHelper;
import com.midori.utils.Auth;
import com.midori.utils.MsgBox;
import com.midori.views.ViewDangNhap;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

public class Application extends JFrame {
    public static Application app;
    public static Map<String,String> config = Auth.getConfig();
    static final String DB_URL = "jdbc:sqlserver://"+config.get("server")+":"
            +config.get("port")+";databaseName="
            +config.get("databaseName")+";trustServerCertificate=true;encrypt=true";
    static final String USER = config.get("username");
    static final String PASS = config.get("password");

    public static void main(String[] args) {
        if (Auth.createDefaultConfigFile()) {
            try {
                DriverManager.getConnection(DB_URL, USER, PASS).close();

                SwingUtilities.invokeLater(() -> {
                    try {
                        UIManager.setLookAndFeel(new FlatLightLaf());
                        UIManager.put("Button.arc", 10);
                        UIManager.put("Component.arc", 10);
                        UIManager.put("ProgressBar.arc", 10);
                        UIManager.put("TextComponent.arc", 10);
                    } catch (UnsupportedLookAndFeelException e) {
                        throw new RuntimeException(e);
                    }
                    app = new Application();
                    app.setContentPane(new ViewDangNhap(app));
                    app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    app.pack();
                    app.setLocationRelativeTo(null);
                    app.setVisible(true);
                });
            } catch (SQLException e) {
                MsgBox.alert(null, "Không thể kết nối CSDL, vui lòng kiểm tra lại file " + Auth.isConfigFileCreated() + "!");
                System.exit(0);
                throw new RuntimeException();
            }
        } else {
            MsgBox.alert(null, "Đã tạo file config.yaml tại đường đẫn " + Auth.isConfigFileCreated() + ", vui lòng cấu hình file và chạy lại chương trình!");
            System.exit(0);
        }
    }
}
