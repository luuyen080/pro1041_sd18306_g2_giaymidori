package com.midori.views.cart;

import com.jhlabs.image.ImageUtils;
import com.midori.utils.Img;
import org.jdesktop.swingx.JXTextArea;

import javax.swing.*;

public class TenSanPham extends JPanel {
    private JPanel p;
    private JXTextArea lblTen;
    private JPanel image;

    public TenSanPham(String icon, String name) {
        image.add(Img.getCircleImage(icon, 30, 0,null,0));
        lblTen.setText(name);
        if (name.length() > 60) lblTen.setText(name.substring(0, 60));
    }

    private void createUIComponents() {
        p = this;
    }
}
