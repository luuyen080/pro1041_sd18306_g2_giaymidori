package com.midori.views.cart;

import com.formdev.flatlaf.ui.FlatBorder;
import com.formdev.flatlaf.ui.FlatButtonBorder;
import com.midori.models.GiayChiTiet;
import com.midori.utils.Cart;
import com.midori.utils.CurrencyConverter;
import lombok.Getter;
import lombok.Setter;
import org.jdesktop.swingx.JXTable;

import javax.swing.*;
import java.awt.*;

@Getter
@Setter
public class CartModel {
    private int id;
    private GiayChiTiet giayChiTiet;
    private CustomSpinner soLuong;
    private long giaBan;
    private JButton xoa;

    public CartModel(GiayChiTiet giayChiTiet, JXTable table, JLabel lblTT) {
        this.giayChiTiet = giayChiTiet;
        id = giayChiTiet.getIdGiayCT();
        giaBan = giayChiTiet.getGiaGoc();
        soLuong = new CustomSpinner();
        xoa = new JButton("x");

        FlatBorder flatBorder = new FlatButtonBorder() {
            @Override
            protected boolean isCellEditor(Component c) {
                return false;
            }
        };

        soLuong.setBorder(flatBorder);
        soLuong.setCursor(new Cursor(Cursor.HAND_CURSOR));
        soLuong.addChangeListener((e) -> Cart.tinhTien(table, lblTT));

        xoa.setBorder(flatBorder);
        xoa.setCursor(new Cursor(Cursor.HAND_CURSOR));
        xoa.addActionListener((e) -> {
            Cart.getCart().remove(this);
            Cart.tinhTien(table, lblTT);
        });
    }
}
