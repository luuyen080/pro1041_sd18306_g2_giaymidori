package com.midori.views.dialogs;

import com.midori.models.DonHang;
import com.midori.utils.CurrencyConverter;
import com.midori.utils.Img;
import org.jdesktop.swingx.JXTable;

import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HoaDonDialog extends JDialog {
    private JPanel contentPane;
    private JLabel lblNguoiTao;
    private JLabel lblTenKH;
    private JXTable table;
    private JLabel lblTPT;
    private JLabel lblTT;
    private JLabel lblMHD;
    private JLabel lblNgayTao;
    private JPanel sp;
    private DefaultTableModel tableModel;

    public HoaDonDialog(DonHang donHang) throws FileNotFoundException {
        table = new JXTable();
        tableModel = (DefaultTableModel) table.getModel();

        lblTenKH.setText("Tên khách hàng: " + donHang.getKh().getTenKhachHang());
        lblNguoiTao.setText("Người tạo: " + donHang.getNv().getTenNhanVien());

        lblMHD.setText("Mã HĐ: #" + donHang.getId());
        lblNgayTao.setText("Ngày tạo: "
                + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
                + " "
                + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")));

        table.setVisibleRowCount(donHang.getGioHangModels().size());
        String[] columnNames = {"Sản phẩm", "SL", "Đơn giá", "Thành tiền"};
        tableModel.setDataVector(donHang.getGioHangModels().stream().map(i -> new Object[]{
                i.getGiayChiTiet().getGiay().getTenGiay() + " - màu " + i.getGiayChiTiet().getMauSac() + " - " + i.getGiayChiTiet().getDeGiay() + " - size " + i.getGiayChiTiet().getKichCo(),
                i.getSoLuong().getValue(),
                CurrencyConverter.parseString(i.getGiayChiTiet().getGiaGoc()),
                CurrencyConverter.parseString(i.getGiayChiTiet().getGiaGoc() * (int) i.getSoLuong().getValue())
        }).toArray(Object[][]::new), columnNames);
        table.setRowHeight(67);
        table.setDefaultRenderer(Object.class, new TextAreaCellRenderer());
        table.packAll();

        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
//        System.out.println(scrollPane.getPreferredSize().getHeight());
        lblTPT.setText(CurrencyConverter.parseString(donHang.getTongTien()));
        sp.add(scrollPane);

        setContentPane(contentPane);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);

        BufferedImage hoadon = new BufferedImage(contentPane.getWidth(), contentPane.getHeight(),
                BufferedImage.TYPE_INT_RGB);
        Graphics2D g2D = (Graphics2D) hoadon.getGraphics();
        contentPane.paint(g2D);
        g2D.translate(0, this.getHeight());
        contentPane.paint(g2D);
        Img.saveBufferImageToFile(hoadon, "invoices/" + donHang.getId() + ".png");


        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream("images/invoices/" + donHang.getId() + ".png");
        } catch (FileNotFoundException ex) {
            throw new RuntimeException(ex);
        }
        DocFlavor myFormat = DocFlavor.INPUT_STREAM.PNG;

        Doc myDoc = new SimpleDoc(fileInputStream, myFormat, null);
        PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
        aset.add(new Copies(1));
        aset.add(OrientationRequested.REVERSE_PORTRAIT);
        aset.add(Sides.ONE_SIDED);

        PrintService[] services = PrintServiceLookup.lookupPrintServices(myFormat, aset);
        if (services.length != 0) {
            DocPrintJob printJob = services[0].createPrintJob();
            try {
                printJob.print(myDoc, aset);
            } catch (PrintException pe) {

            }
        }
    }
    class TextAreaCellRenderer extends JTextArea implements TableCellRenderer {

        private final List<List<Integer>> rowAndCellHeights = new ArrayList<>();

        public TextAreaCellRenderer() {
            setColumns(25);
            setWrapStyleWord(true);
            setLineWrap(true);
            setOpaque(true);
            setBorder(new EmptyBorder(8, 10, 8, 10));
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            if (column != 0) return new JLabel(value.toString());
            setText(Objects.toString(value, ""));
            adjustRowHeight(table, row, column);
            setFont(table.getFont());
            System.out.println(this.getHeight());
            return this;
        }

        private void adjustRowHeight(JTable table, int row, int column) {
            setBounds(table.getCellRect(row, column, false));
            int preferredHeight = getPreferredSize().height;
            while (rowAndCellHeights.size() <= row) {
                rowAndCellHeights.add(new ArrayList<>(column));
            }
            List<Integer> list = rowAndCellHeights.get(row);
            while (list.size() <= column) {
                list.add(0);
            }
            list.set(column, preferredHeight);
            int max = list.stream().max((x, y) -> Integer.compare(x, y)).get();
            if (table.getRowHeight(row) != max) {
                table.setRowHeight(row, max);
            }
        }
    }
}
