package com.midori.views;

import com.formdev.flatlaf.FlatClientProperties;
import com.midori.Application;
import com.midori.models.Giay;
import com.midori.services.*;
import com.midori.services.Impl.DonHangServices;
import com.midori.services.Impl.GiayChiTietServices;
import com.midori.services.Impl.KhachHangServices;
import com.midori.services.Impl.NhanVienServices;
import com.midori.utils.Auth;
import com.midori.utils.Img;
import com.midori.utils.MsgBox;
import com.midori.views.components.menu.CustomMenu;
import com.midori.views.components.menu.MenuModel;
import com.midori.views.panel.*;
import org.jdesktop.swingx.JXDatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.List;

public class ViewQL extends JPanel {
    private JPanel panelQuanLy;
    private JPanel content;
    private JList listMenu;
    private JLabel lblUsername;
    private JButton btnDangXuat;
    private JLabel lblLogo;
    private JTextField txtSoLuong;
    private JSpinner listKichCo;
    private JRadioButton rdoConHang;
    private JRadioButton rdoHetHang;
    private org.jdesktop.swingx.JXTable tblDanhSachSanPham;
    private JComboBox cboTenGiay;
    private JTextField txtMaGiayCT;
    private JTextField txtGiaNhap;
    private JTextField txtGiaGoc;
    private JTextField txtDeGiay;
    private JXDatePicker txtNgayThem;
    private JXDatePicker txtNgaySua;
    private JButton btnThem;
    private JButton btnSua;
    private JButton btnXoa;
    private JButton btnReset;
    private JList listMauSac;
    private JTextField txtTimKiemGCT;
    private SanPhamQLView sanPhamQL;
    private JButton btnSanPham;
    private DefaultListModel<MenuModel> listModel;
    private DefaultTableModel tableModel;
    private DefaultComboBoxModel<Giay> comboBoxModel;
    private IGiayChiTietServices serviceGiayChiTiet;
    private IGiayServices serviceGiay;
    private INhanVienServices nhanVienServices;
    private IKhachHangServices khachHangServices;
    private IDonHangServices donHangServices;
    private SanPhamChiTietQL sanPhamChiTietQL;
    private TongQuanView tongQuanView;
    private KhachHangQLView khachHangQLView;
    private NhanVienQLView nhanVienQLView;
    private DonHangQLView donHangQLView;
    private ThongKeView thongKeView;

    public ViewQL(Application application) {
        initComponent();

        listMenu.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                chooseItemMenu();
            }
        });
        btnDangXuat.addActionListener((e) -> logout(application));
    }

    public void initComponent() {
        lblLogo.setIcon(new ImageIcon(Img.readImage("logo.png").getScaledInstance(150, 150, Image.SCALE_SMOOTH)));

        Auth.content = content;

        serviceGiayChiTiet = new GiayChiTietServices();
        nhanVienServices = new NhanVienServices();
        khachHangServices = new KhachHangServices();
        donHangServices = new DonHangServices();

        lblUsername.setText("Xin chào, " + Auth.user.getTenNhanVien());
        listModel = new DefaultListModel<>();
        listMenu.setModel(listModel);

        fillListMenu(List.of(
//                new MenuModel("tongquan-w.svg", "tongquan-b.svg", "Tổng quan"),
                new MenuModel("sanpham-w.svg", "sanpham-b.svg", "Sản phẩm"),
                new MenuModel("khachhang-w.svg", "khachhang-b.svg", "Khách hàng"),
                new MenuModel("nhanvien-w.svg", "nhanvien-b.svg", "Nhân viên"),
                new MenuModel("donhang-w.svg", "donhang-b.svg", "Đơn hàng"),
                new MenuModel("thongke-w.svg", "thongke-b.svg", "Thống kê")
        ));
        listMenu.setSelectedIndex(0);

        btnDangXuat.setIcon(Img.readSVG("dangxuat-w.svg"));
        btnDangXuat.setIconTextGap(25);
        btnDangXuat.setHorizontalAlignment(SwingConstants.LEFT);
        btnDangXuat.putClientProperty(FlatClientProperties.STYLE, "arc:0");
    }

    public void fillListMenu(List<MenuModel> list) {
        listModel.clear();
        for (MenuModel menuModel : list) listModel.addElement(menuModel);
    }

    private void chooseItemMenu() {
        content.removeAll();
        switch (listMenu.getSelectedIndex()) {
//            case 0 -> {
//                tongQuanView = new TongQuanView();
//                content.add(tongQuanView);
//            }
            case 0 -> {
                sanPhamChiTietQL = new SanPhamChiTietQL();
                content.add(sanPhamChiTietQL);
            }
            case 1 -> {
                khachHangQLView = new KhachHangQLView();
                content.add(khachHangQLView);
            }
            case 2 -> {
                nhanVienQLView = new NhanVienQLView();
                content.add(nhanVienQLView);
            }
            case 3 -> {
                donHangQLView = new DonHangQLView();
                content.add(donHangQLView);
            }
            case 4 -> {
                thongKeView = new ThongKeView();
                content.add(thongKeView);
            }
        }
        content.revalidate();
    }

    private void logout(Application application) {
        if (MsgBox.confirm(null, "Bạn có muốn đăng xuất không?")) {
            Auth.clear();
            application.setContentPane(new ViewDangNhap(application));
            application.pack();
            application.setLocationRelativeTo(null);
        }
    }

    private void createUIComponents() {
        panelQuanLy = this;
        listMenu = new CustomMenu();
    }
}
