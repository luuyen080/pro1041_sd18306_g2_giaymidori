package com.midori.views;

import com.midori.models.NhanVien;
import com.midori.services.INhanVienServices;
import com.midori.services.Impl.NhanVienServices;
import com.midori.utils.Auth;
import com.midori.utils.MailService;
import com.midori.utils.MsgBox;
import com.midori.views.dialogs.Loading;

import javax.mail.MessagingException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ViewQuenMatKhau extends JDialog {
    private JPanel contentPane;
    private JPanel cardPane;
    private JPanel otp;
    private JButton btnSendOTP;
    private JPanel email;
    private JButton btnSubmit;
    private JTextField txtEmail;
    private JPasswordField txtPass;
    private JPasswordField txtRePass;
    private JTextField txtOTP;
    private INhanVienServices nhanVienServices = new NhanVienServices();
    private NhanVien nhanVien;
    private Timer timer;
    private int secondsRemaining;
    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    public ViewQuenMatKhau() {
        initComponent();

        timer = new Timer(60000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                enableSendButton();
            }
        });
    }

    private void initComponent() {
        setContentPane(contentPane);
        setModal(true);
        pack();
        setLocationRelativeTo(null);
        btnSendOTP.setCursor(new Cursor(Cursor.HAND_CURSOR));
        btnSubmit.setCursor(new Cursor(Cursor.HAND_CURSOR));
        btnSubmit.addActionListener(e -> changePass());
        btnSendOTP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Kiểm tra xem đã hết thời gian chưa
                if (!timer.isRunning()) {
                    // Gửi OTP và bắt đầu đếm ngược
                    nhanVien = sendOtp();
                    System.out.println(Auth.otp);
                    if (nhanVien != null) startTimer();
                } else {
                    // Thông báo người dùng chờ cho đến khi hết thời gian
                    JOptionPane.showMessageDialog(null, "Vui lòng chờ đến khi hết thời gian trước khi gửi lại OTP.");
                }
            }
        });
    }

    private void startTimer() {
        // Disable nút để ngăn chặn việc nhấn liên tục trong thời gian chờ
        disableSendButton();
        secondsRemaining = 60;
        timer.start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (secondsRemaining > 0) {
                    try {
                        btnSendOTP.setText(String.valueOf(secondsRemaining));
                        // Đợi 1 giây
                        Thread.sleep(1000);
                        secondsRemaining--;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                timer.stop();
                Auth.otp = null;
                btnSendOTP.setText("Gửi OTP");
            }
        }).start();
    }

    private void enableSendButton() {
        btnSendOTP.setEnabled(true);
    }

    private void disableSendButton() {
        btnSendOTP.setEnabled(false);
    }

    private void changePass() {
        String pass = new String(txtPass.getPassword());
        String rePass = new String(txtRePass.getPassword());
        String otp = txtOTP.getText();
        if (!pass.trim().isEmpty() || !rePass.trim().isEmpty() || !otp.trim().isEmpty() || !txtEmail.getText().isEmpty()) {
            if (nhanVien != null) {
                if (pass.equals(rePass)) {
                    if (otp.equals(Auth.otp)) {
                        nhanVien.setPassword(pass);
                        nhanVienServices.updateUsernamePassword(nhanVien);
                        MsgBox.alert(null, "Đổi mật khẩu thành công!");
                        this.dispose();
                    } else MsgBox.alert(null, "Sai OTP");
                } else MsgBox.alert(null, "Không trùng mật khẩu");
            } else MsgBox.alert(null, "Vui lòng gửi OTP");
        } else MsgBox.alert(null, "Vui lòng nhập đủ thông tin");
    }

    private NhanVien sendOtp() {
        Loading loading = new Loading();
        executorService.submit(() -> {
            try {
                if (!txtEmail.getText().trim().isEmpty()) {
                    if (Auth.isValidEmail(txtEmail.getText())) {
                        nhanVien = nhanVienServices.findByEmail(txtEmail.getText());
                    }
                    MailService.sendEmail(nhanVien.getEmail());
                    loading.dispose();
                    MsgBox.alert(null, "Mail đã được gửi tới email: " + nhanVien.getEmail());
                } else {
                    loading.dispose();
                    MsgBox.alert(null, "Vui lòng nhập email!");
                }
            } catch (MessagingException e) {
                loading.dispose();
                MsgBox.alert(null, "Email ko hợp lệ!");
            } catch (SQLException e) {
                loading.dispose();
                MsgBox.alert(null, "Không tồn tại email!");
            }
        });
        loading.setVisible(true);
        return nhanVien;
    }
}
