package com.midori.views.panel;

import com.formdev.flatlaf.FlatClientProperties;
import com.midori.models.*;
import com.midori.services.IDonHangServices;
import com.midori.services.IGiayChiTietServices;
import com.midori.services.IKhachHangServices;
import com.midori.services.INhanVienServices;
import com.midori.services.Impl.DonHangServices;
import com.midori.services.Impl.GiayChiTietServices;
import com.midori.services.Impl.KhachHangServices;
import com.midori.services.Impl.NhanVienServices;
import com.midori.utils.*;
import com.midori.views.cart.CartModel;
import com.midori.views.cart.CustomCart;
import com.midori.views.dialogs.HoaDonDialog;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SPCT_NVView extends JPanel {
    private JButton btnThemGH;
    private org.jdesktop.swingx.JXTable tableGH;
    private JButton btnThanhToan;
    private JPanel panelSanPhamChiTietNV;
    private org.jdesktop.swingx.JXTable tableSP;
    private JLabel lblTongTien;
    private JComboBox cboKhachHang;
    private JTextField txtTK;
    private JPanel anh;
    private DefaultTableModel tableModelSP;
    private DefaultComboBoxModel<KhachHang> comboBoxModel;
    private IGiayChiTietServices giayChiTietServices;
    private IKhachHangServices khachHangServices;
    private IDonHangServices donHangServices;
    private GiayChiTiet giayChiTiet;

    public SPCT_NVView() {
        tableSP.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            if (tableSP.getSelectedRow() >= 0) showItem(tableSP.getSelectedRow());
            }
        });
        btnThemGH.addActionListener((e) -> {
            if (tableSP.getSelectedRow() >= 0) addToCart();
            else MsgBox.alert(null, "Vui lòng chọn sản phẩm trước khi ấn thêm vào giỏ hàng!");
        });
        btnThanhToan.addActionListener((e) -> saveDonHang());
        txtTK.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                search();
            }
        });
        initComponent();
    }

    private void initComponent() {
        giayChiTietServices = new GiayChiTietServices();
        khachHangServices = new KhachHangServices();
        donHangServices = new DonHangServices();

        txtTK.putClientProperty(
                FlatClientProperties.PLACEHOLDER_TEXT,
                "Nhập mã hoặc tên giày...");
        txtTK.putClientProperty(
                FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON,
                true);

        fillComboboxKhachHang();

        tableModelSP = (DefaultTableModel) tableSP.getModel();
        tableModelSP.setColumnIdentifiers(new String[]{"Mã giày CT", "Tên giày", "Ảnh", "Màu sắc", "Giá gốc", "Số lượng", "Kích cỡ", "Đế giày", "Ngày thêm", "Ngày sửa", "Trạng thái"});
        fillTableSP(giayChiTietServices.findAllByTrangThai());

        if (tableModelSP.getRowCount() > 0) {
            showItem(0);
            tableSP.setRowSelectionInterval(0, 0);
        }
    }

    private void fillComboboxKhachHang() {
        try {
            comboBoxModel = new DefaultComboBoxModel<>();
            cboKhachHang.setModel(comboBoxModel);
            for (KhachHang khachHang : khachHangServices.findAll()) {
                comboBoxModel.addElement(khachHang);
            }
        } catch (Exception e) {
            MsgBox.alert(null, e.getMessage());
        }
    }

    public void fillTableSP(List<GiayChiTiet> list) {
        tableModelSP.setRowCount(0);
        for (GiayChiTiet giayChiTiet : list) {
            tableModelSP.addRow(giayChiTiet.toObject());
        }
        tableSP.setDefaultEditor(Object.class, null);
        tableSP.packAll();
    }

    private void showItem(int index) {
        int id = (int) tableSP.getValueAt(index, 0);
        GiayChiTiet gct = giayChiTietServices.findById(id);
        anh.removeAll();
        anh.add(Img.getCircleImage(gct.getAnh(), 200, 0, null, 0));
        anh.revalidate();
    }

    public void addToCart() {
        int id = (int) tableSP.getValueAt(tableSP.getSelectedRow(), 0);
        giayChiTiet = giayChiTietServices.findById(id);
        Optional<CartModel> cartModel = Cart.getCart().stream().filter(i -> i.getId() == giayChiTiet.getIdGiayCT()).findFirst();
        if (cartModel.isEmpty()) {
            Cart.getCart().add(new CartModel(giayChiTiet, tableGH, lblTongTien));
        } else {
            JSpinner s = cartModel.get().getSoLuong();
            s.setValue(s.getNextValue());
            tableGH.repaint();
        }
        Cart.tinhTien(tableGH, lblTongTien);
        ((CustomCart) tableGH).initComponent();
        tableGH.packAll();
    }


    public DonHang readForm() {
        try {
            INhanVienServices nhanVienServices = new NhanVienServices();
            KhachHang khachHang = (KhachHang) cboKhachHang.getSelectedItem();
            NhanVien nhanVien = nhanVienServices.findById(Auth.user.getId());
            long tongTien = CurrencyConverter.parseLong(lblTongTien.getText());
            return new DonHang (khachHang, nhanVien, tongTien, new Timestamp(System.currentTimeMillis()), Cart.getCart());
        } catch (Exception e) {
            MsgBox.alert(null, e.getMessage());
        }
        return null;
    }

    private void saveDonHang() {
        try {
            DonHang donHang = readForm();
            if (!donHang.getGioHangModels().isEmpty()) {
                donHangServices.save(donHang);
                new HoaDonDialog(donHang).setVisible(true);
                fillTableSP(giayChiTietServices.findAllByTrangThai());
                clearForm();
            } else MsgBox.alert(null, "Vui lòng thêm sản phẩm vào giỏ hàng!");
        } catch (Exception e) {
            MsgBox.alert(null, e.getMessage());
        }
    }

    private void clearForm() {
        Cart.getCart().clear();
        cboKhachHang.setSelectedIndex(0);
        lblTongTien.setText("0");
        anh.removeAll();
        anh.add(Img.getCircleImage("no-image.png", 200, 0, null, 0));
        anh.revalidate();
        tableSP.clearSelection();
    }

    private void search() {
        String keyword = txtTK.getText();
        if (!keyword.isEmpty()) {
            try {
                fillTableSP(giayChiTietServices.selectByKeyword(keyword));
            } catch (SQLException e) {
                MsgBox.alert(null, e.getMessage());
            }
        }
        else {
            fillTableSP(giayChiTietServices.findAll());
        }
    }

    private void createUIComponents() {
        panelSanPhamChiTietNV = this;
        tableGH = new CustomCart(Cart.getCart());
    }
}
