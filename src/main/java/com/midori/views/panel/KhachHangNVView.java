package com.midori.views.panel;

import com.formdev.flatlaf.FlatClientProperties;
import com.midori.models.KhachHang;
import com.midori.services.IKhachHangServices;
import com.midori.services.Impl.KhachHangServices;
import com.midori.utils.MsgBox;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public class KhachHangNVView extends JPanel {
    private JTextField txtMaKhachHang;
    private JTextField txtTenKhachHang;
    private JTextField txtSDT;
    private JTextField txtDiemTichLuy;
    private JRadioButton rdoNam;
    private JRadioButton rdoNu;
    private org.jdesktop.swingx.JXTable table;
    private JPanel panelKhachHangNV;
    private JButton btnThem;
    private JButton btnSua;
    private JButton btnXoa;
    private JButton btnReset;
    private JTextField txtTK;
    private DefaultTableModel tableModel;
    private IKhachHangServices khachHangServices;
    private int index = -1;

    public KhachHangNVView() {
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            index = table.getSelectedRow();
            if (index >= 0) showItem(index);
            }
        });
        btnThem.addActionListener((e) -> insert());
        btnSua.addActionListener((e) -> update());
        btnXoa.addActionListener((e) -> delete());
        btnReset.addActionListener((e) -> clearForm());
        txtTK.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                search();
            }
        });
        initComponent();
    }

    private void initComponent() {
        khachHangServices = new KhachHangServices();

        txtTK.putClientProperty(
                FlatClientProperties.PLACEHOLDER_TEXT,
                "Nhập tên hoặc số điện thoại...");
        txtTK.putClientProperty(
                FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON,
                true);

        tableModel = (DefaultTableModel) table.getModel();
        tableModel.setColumnIdentifiers(new String[]{"Mã khách hàng", "Tên khách hàng", "Giới tính", "Số điện thoại", "Điểm tích luỹ", "Ngày thêm"});
        fillTable(khachHangServices.findAll());

        if (tableModel.getRowCount() > 0) {
            showItem(0);
            table.setRowSelectionInterval(0, 0);
        }
    }


    public void fillTable(List<KhachHang> list) {
        tableModel.setRowCount(0);
        for (KhachHang khachHang : list) {
            tableModel.addRow(khachHang.toObject());
        }
        table.setDefaultEditor(Object.class, null);
        table.packAll();
    }

    private void showItem(int index) {
        try {
            int id = (int) tableModel.getValueAt(index, 0);
            KhachHang khachHang = khachHangServices.findById(id);
            txtMaKhachHang.setText(String.valueOf(khachHang.getIdKhachHang()));
            txtTenKhachHang.setText(khachHang.getTenKhachHang());
            txtSDT.setText(khachHang.getSdt());
            txtDiemTichLuy.setText(String.valueOf(khachHang.getDiemTichLuy()));
            if (khachHang.isGioiTinh()) rdoNam.setSelected(true);
            else rdoNu.setSelected(true);
        } catch (Exception e) {
            MsgBox.alert(null, e.getMessage());
        }
    }

    private void clearForm() {
        txtMaKhachHang.setText("");
        txtTenKhachHang.setText("");
        txtSDT.setText("");
        txtDiemTichLuy.setText("");
        rdoNam.setSelected(true);
        index = -1;
        table.clearSelection();
    }

    private KhachHang readForm() {
        String ten, sdt;
        int diemTichLuy;
        boolean gioiTinh;
        ten = txtTenKhachHang.getText().trim();
        if (ten.isEmpty()) {
            MsgBox.alert(null, "Không được để trống tên, vui lòng nhập tên");
            return null;
        }
        if (ten.matches(".*\\d.*") || ten.matches(".*[^\\p{L}0-9_\\s].*")) {
            MsgBox.alert(null, "Tên không được chứa số hoặc kí tự đặc biệt");
            return null;
        }
        sdt = txtSDT.getText().trim();
        if (!sdt.isEmpty() && !sdt.matches("^0\\d{9}$")) {
            MsgBox.alert(null, "Số điện thoại phải bắt đầu bằng số 0 và có tổng cộng 10 số");
            return null;
        }
        String diemTichLuyStr = txtDiemTichLuy.getText().trim();
        if (diemTichLuyStr.isEmpty()) {
            MsgBox.alert(null, "Điểm tích lũy không được để trống");
            return null;
        }
        if (!diemTichLuyStr.matches("^\\d+$")) {
            MsgBox.alert(null, "Điểm tích lũy không hợp lệ");
            return null;
        }
        try {
            diemTichLuy = Integer.parseInt(diemTichLuyStr);
            if (diemTichLuy < 0) {
                MsgBox.alert(null, "Điểm tích lũy phải là số nguyên dương");
                return null;
            }
        } catch (NumberFormatException e) {
            MsgBox.alert(null, "Điểm tích lũy không hợp lệ");
            return null;
        }

        if (rdoNam.isSelected()) gioiTinh = true;
        else gioiTinh = false;
        return new KhachHang(ten, sdt, diemTichLuy, new Timestamp(System.currentTimeMillis()), gioiTinh);
    }

    private void insert() {
        KhachHang khachHang = readForm();
        if (khachHang != null) {
            try {
                MsgBox.alert(null, khachHangServices.save(khachHang));
                fillTable(khachHangServices.findAll());
                clearForm();
            } catch (Exception e) {
                MsgBox.alert(null, e.getMessage());
            }
        }
    }

    private void update() {
        if (MsgBox.confirm(null, "Bạn có muốn sửa không?")) {
            KhachHang khachHang = readForm();
            if (khachHang != null && index >= 0) {
                try {
                    khachHang.setIdKhachHang((int) table.getValueAt(index, 0));
                    MsgBox.alert(null, khachHangServices.update(khachHang));
                    fillTable(khachHangServices.findAll());
                    clearForm();
                } catch (Exception e) {
                    MsgBox.alert(null, e.getMessage());
                }
            }
        }
    }

    private void delete() {
        if (MsgBox.confirm(null, "Bạn có muốn xoá không?")) {
            int id = (int) table.getValueAt(index, 0);
            try {
                MsgBox.alert(null, khachHangServices.deleteById(id));
                fillTable(khachHangServices.findAll());
                clearForm();
            } catch (Exception e) {
                MsgBox.alert(null, e.getMessage());
            }
        }
    }

    private void search() {
        String keyword = txtTK.getText();
        if (!keyword.isEmpty()) {
            try {
                fillTable(khachHangServices.selectByKeyword(keyword));
            } catch (SQLException e) {
                MsgBox.alert(null, e.getMessage());
            }
        }
        else {
            fillTable(khachHangServices.findAll());
        }
        clearForm();
    }

    private void createUIComponents() {
        panelKhachHangNV = this;
    }
}
