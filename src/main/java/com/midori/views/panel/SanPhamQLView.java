package com.midori.views.panel;

import com.formdev.flatlaf.FlatClientProperties;
import com.midori.models.Giay;
import com.midori.services.IGiayChiTietServices;
import com.midori.services.IGiayServices;
import com.midori.services.Impl.GiayChiTietServices;
import com.midori.services.Impl.GiayServices;
import com.midori.utils.Auth;
import com.midori.utils.MsgBox;
import lombok.Setter;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public class SanPhamQLView extends JPanel {
    private SanPhamChiTietQL sanPhamChiTietQL;
    private JTextField txtMaGiay;
    private JTextField txtTenGiay;
    private JButton btnThem;
    private JButton btnSua;
    private JButton btnXoa;
    private JButton btnReset;
    private JButton btnQLGiayCT;
    private org.jdesktop.swingx.JXTable table;
    private JPanel panelSanPhamQLView;
    private JComboBox cboThuongHieu;
    private JTextField txtTK;
    private DefaultTableModel tableModel;
    private IGiayChiTietServices giayChiTietServices;
    private IGiayServices giayServices;
    private int index = -1;

    public SanPhamQLView() {
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            index = table.getSelectedRow();
            if (index >= 0) showItem(index);
            }
        });
        btnThem.addActionListener((e) -> insert());
        btnSua.addActionListener((e) -> update());
        btnXoa.addActionListener((e) -> delete());
        btnReset.addActionListener((e) -> clearForm());
        btnQLGiayCT.addActionListener((e) -> xemSPCT());
        txtTK.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                search();
            }
        });
        initComponent();
    }

    private void initComponent() {
        giayChiTietServices = new GiayChiTietServices();
        giayServices = new GiayServices();

        txtTK.putClientProperty(
                FlatClientProperties.PLACEHOLDER_TEXT,
                "Nhập mã, tên hoặc thương hiệu giày...");
        txtTK.putClientProperty(
                FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON,
                true);

        tableModel = (DefaultTableModel) table.getModel();
        tableModel.setColumnIdentifiers(new String[]{"Mã giày", "Tên giày", "Thương hiệu", "Ngày thêm", "Ngày sửa"});
        fillTable(giayServices.findAll());

        if (tableModel.getRowCount() > 0) {
            showItem(0);
            table.setRowSelectionInterval(0, 0);
        }
    }

    public void fillTable(List<Giay> list) {
        tableModel = (DefaultTableModel) table.getModel();
        tableModel.setRowCount(0);
        for (Giay giay : list) {
            tableModel.addRow(giay.toObject());
        }
        table.setDefaultEditor(Object.class, null);
        table.packAll();
    }

    private void showItem(int index) {
        try {
            int id = (int) tableModel.getValueAt(index, 0);
            Giay giay = giayServices.findById(id);
            txtMaGiay.setText(String.valueOf(giay.getIdGiay()));
            txtTenGiay.setText(giay.getTenGiay());
            cboThuongHieu.setSelectedItem(giay.getThuongHieu());
        } catch (Exception e) {
            MsgBox.alert(null, e.getMessage());
        }
    }

    private void clearForm() {
        txtMaGiay.setText("");
        txtTenGiay.setText("");
        cboThuongHieu.setSelectedIndex(0);
        index = -1;
        table.clearSelection();
    }

    private Giay readForm() {
        String ten, thuongHieu;
        ten = txtTenGiay.getText().trim();
        if (ten.isEmpty()) {
            MsgBox.alert(null, "Không được để trống tên, vui lòng nhập tên");
            return null;
        }
        thuongHieu = cboThuongHieu.getSelectedItem().toString();
        return new Giay(ten, thuongHieu, new Timestamp(System.currentTimeMillis()), new Timestamp(System.currentTimeMillis()));
    }

    private void insert() {
        Giay giay = readForm();
        if (giay != null) {
            try {
                MsgBox.alert(null, giayServices.save(giay));
                fillTable(giayServices.findAll());
            } catch (Exception e) {
                MsgBox.alert(null, e.getMessage());
            }
        }
    }

    private void update() {
        if (MsgBox.confirm(null, "Bạn có muốn sửa không?")) {
            Giay giay = readForm();
            if (giay != null && index >= 0) {
                try {
                    giay.setIdGiay((int) table.getValueAt(index, 0));
                    MsgBox.alert(null, giayServices.update(giay));
                    fillTable(giayServices.findAll());
                    clearForm();
                } catch (Exception e) {
                    MsgBox.alert(null, e.getMessage());
                }
            }
        }
    }

    private void delete() {
        if (MsgBox.confirm(null, "Bạn có muốn xoá không?")) {
            int id = (int) table.getValueAt(index, 0);
            try {
                MsgBox.alert(null, giayServices.deleteById(id));
                fillTable(giayServices.findAll());
                clearForm();
            } catch (Exception e) {
                MsgBox.alert(null, e.getMessage());
            }
        }
    }

    private void xemSPCT() {
        sanPhamChiTietQL = new SanPhamChiTietQL();
        Auth.content.removeAll();
        Auth.content.add(sanPhamChiTietQL);
        Auth.content.revalidate();
    }

    private void search() {
        String keyword = txtTK.getText();
        if (!keyword.isEmpty()) {
            try {
                fillTable(giayServices.selectByKeyword(keyword));
            } catch (SQLException e) {
                MsgBox.alert(null, e.getMessage());
            }
        }
        else {
            fillTable(giayServices.findAll());
        }
        clearForm();
    }

    private void createUIComponents() {
        panelSanPhamQLView = this;
    }
}
