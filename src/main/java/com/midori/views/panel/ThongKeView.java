package com.midori.views.panel;

import com.midori.models.DoanhThuNam;
import com.midori.models.DoanhThuNgay;
import com.midori.models.SanPhamNam;
import com.midori.models.SanPhamNgay;
import com.midori.services.IThongKeServices;
import com.midori.services.Impl.ThongKeServices;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardXYItemLabelGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.List;

public class ThongKeView extends JPanel {
    private JPanel panelThongKe;
    private JPanel contentTT;
    private JRadioButton rdoSanPham;
    private JRadioButton rdoDoanhThu;
    private JPanel contentTN;
    private JRadioButton rdoSanPhamTN;
    private JRadioButton rdoDoanhThuTN;
    private JComboBox cboNamTN;
    private JComboBox cboNamTT;
    private JComboBox cboThang;
    private IThongKeServices thongKeService = new ThongKeServices();

    public ThongKeView() {
        cboThang.addActionListener((e) -> hienThiTheoThang());
        cboNamTT.addActionListener((e) -> hienThiTheoThang());
        rdoDoanhThu.addActionListener((e) -> hienThiTheoThang());
        rdoSanPham.addActionListener((e) -> hienThiTheoThang());

        cboNamTN.addActionListener((e) -> hienThiTheoNam());
        rdoDoanhThuTN.addActionListener((e) -> hienThiTheoNam());
        rdoSanPhamTN.addActionListener((e) -> hienThiTheoNam());

        initComponent();
    }

    private void initComponent() {
        int thang = LocalDateTime.now().getMonthValue();
        int nam = LocalDateTime.now().getYear();
        cboThang.setSelectedItem(thang < 10 ? "0" + thang : String.valueOf(thang));
        cboNamTT.setSelectedItem(String.valueOf(nam));
        cboNamTN.setSelectedItem(String.valueOf(nam));
        hienThiTheoThang();
        hienThiTheoNam();
    }

    private void fillBieuDo(XYDataset dataset, JPanel content) {
        JFreeChart chart = createSplineChart(dataset);

        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        chartPanel.setBackground(Color.WHITE);

        content.removeAll();
        content.add(chartPanel);
        content.revalidate();
    }

    private JFreeChart createSplineChart(XYDataset dataset) {
        JFreeChart chart = ChartFactory.createXYLineChart(
                "Biểu đồ thống kê doanh thu và sản phẩm",
                "",
                "",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        XYPlot plot = (XYPlot) chart.getPlot();
        NumberAxis xAxis = (NumberAxis) plot.getDomainAxis();
        xAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
//        xAxis.setUpperBound(dataset.getItemCount(0));

        // Tạo định dạng tùy chỉnh cho giá trị trục y
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        NumberFormat format = new DecimalFormat("#,###", symbols);
        NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
        yAxis.setNumberFormatOverride(format);

        plot.setBackgroundPaint(Color.WHITE);
        SplineRenderer renderer = new SplineRenderer();
        renderer.setSeriesShapesVisible(0, true); // Hiển thị điểm dữ liệu cho series 0
        renderer.setSeriesLinesVisible(0, true); // Tắt đường nối giữa các điểm dữ liệu cho series 0
        renderer.setBaseItemLabelGenerator(new CustomXYItemLabelGenerator());
        renderer.setBaseItemLabelsVisible(true);
        renderer.setPrecision(1000);
        plot.setRenderer(renderer);

        return chart;
    }

    private void hienThiTheoThang() {

        int thang = Integer.parseInt(cboThang.getSelectedItem().toString());
        int nam = Integer.parseInt(cboNamTT.getSelectedItem().toString());

        DefaultXYDataset dataset = new DefaultXYDataset();

        List<DoanhThuNgay> listDoanhThu = thongKeService.getAllTongTienTheoThang(thang, nam);
        List<SanPhamNgay> listSanPham = thongKeService.getAllSanPhamTheoThang(thang, nam);
        double[][] data = new double[2][listDoanhThu.size()];

        if (rdoDoanhThu.isSelected()) {
            for (int i = 0; i < listDoanhThu.size(); i++) {
                DoanhThuNgay dto = listDoanhThu.get(i);
                data[0][i] = dto.getNgay();
                data[1][i] = dto.getTongTien();
            }
        }

        if (rdoSanPham.isSelected()) {
            for (int i = 0; i < listSanPham.size(); i++) {
                SanPhamNgay dto = listSanPham.get(i);
                data[0][i] = dto.getNgay();
                data[1][i] = dto.getSoLuong();
            }
        }

        dataset.addSeries("Biểu đồ thống kê " + thang + "/" + nam, data);

        fillBieuDo(dataset, contentTT);
    }

    private void hienThiTheoNam() {

        int nam = Integer.parseInt(cboNamTN.getSelectedItem().toString());

        DefaultXYDataset dataset = new DefaultXYDataset();

        List<DoanhThuNam> listDoanhThu = thongKeService.getAllTongTienTheoNam(nam);
        List<SanPhamNam> listSanPham = thongKeService.getAllSanPhamTheoNam(nam);
        double[][] data = new double[2][listDoanhThu.size()];

        if (rdoDoanhThuTN.isSelected()) {
            for (int i = 0; i < listDoanhThu.size(); i++) {
                DoanhThuNam dto = listDoanhThu.get(i);
                data[0][i] = dto.getThang();
                data[1][i] = dto.getTongTien();
            }
        }

        if (rdoSanPhamTN.isSelected()) {
            for (int i = 0; i < listSanPham.size(); i++) {
                SanPhamNam dto = listSanPham.get(i);
                data[0][i] = dto.getThang();
                data[1][i] = dto.getSoLuong();
            }
        }

        dataset.addSeries("Biểu đồ thống kê năm" + nam, data);

        fillBieuDo(dataset, contentTN);
    }

    private void createUIComponents() {
        panelThongKe = this;
    }

    class CustomXYItemLabelGenerator extends StandardXYItemLabelGenerator {
        private DecimalFormat format;

        public CustomXYItemLabelGenerator() {
            super();
            format = new DecimalFormat("#,###");
        }

        @Override
        public String generateLabel(org.jfree.data.xy.XYDataset dataset, int series, int item) {
            Number y = dataset.getY(series, item);
            if (y != null) {
                double value = y.doubleValue();
                return format.format(value).replace(",", ".");
            }
            return super.generateLabel(dataset, series, item);
        }
    }
}
