package com.midori.views.panel;

import com.formdev.flatlaf.FlatClientProperties;
import com.midori.models.KhachHang;
import com.midori.services.IKhachHangServices;
import com.midori.services.Impl.KhachHangServices;
import com.midori.utils.MsgBox;
import org.jdesktop.swingx.JXTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.List;

public class KhachHangQLView extends JPanel {
    private JPanel panelKhachHangQL;
    private JTextField txtMaKhachHang;
    private JTextField txtTenKhachHang;
    private JTextField txtSDT;
    private JTextField txtDiemTichLuy;
    private JRadioButton rdoNam;
    private JRadioButton rdoNu;
    private JXTable table;
    private JTextField txtTK;
    private DefaultTableModel tableModel;
    private IKhachHangServices khachHangServices;
    private int index = -1;

    public KhachHangQLView() {
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            index = table.getSelectedRow();
            if (index >= 0) showItem(index);
            }
        });
        txtTK.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                search();
            }
        });
        initComponent();
    }

    private void initComponent() {
        khachHangServices = new KhachHangServices();

        txtTK.putClientProperty(
                FlatClientProperties.PLACEHOLDER_TEXT,
                "Nhập tên hoặc số điện thoại...");
        txtTK.putClientProperty(
                FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON,
                true);

        tableModel = (DefaultTableModel) table.getModel();
        tableModel.setColumnIdentifiers(new String[]{"Mã khách hàng", "Tên khách hàng", "Giới tính", "Số điện thoại", "Điểm tích luỹ", "Ngày thêm"});
        fillTable(khachHangServices.findAll());

        if (tableModel.getRowCount() > 0) {
            showItem(0);
            table.setRowSelectionInterval(0, 0);
        }
    }

    public void fillTable(List<KhachHang> list) {
        tableModel.setRowCount(0);
        for (KhachHang khachHang : list) {
            tableModel.addRow(khachHang.toObject());
        }
        table.setDefaultEditor(Object.class, null);
        table.packAll();
    }

    private void showItem(int index) {
        try {
            int id = (int) tableModel.getValueAt(index, 0);
            KhachHang khachHang = khachHangServices.findById(id);
            txtMaKhachHang.setText(String.valueOf(khachHang.getIdKhachHang()));
            txtTenKhachHang.setText(khachHang.getTenKhachHang());
            txtSDT.setText(khachHang.getSdt());
            txtDiemTichLuy.setText(String.valueOf(khachHang.getDiemTichLuy()));
            if (khachHang.isGioiTinh()) rdoNam.setSelected(true);
            else rdoNu.setSelected(true);
        } catch (Exception e) {
            MsgBox.alert(null, e.getMessage());
        }
    }

    private void clearForm() {
        txtMaKhachHang.setText("");
        txtTenKhachHang.setText("");
        txtSDT.setText("");
        txtDiemTichLuy.setText("");
        rdoNam.setSelected(true);
        index = -1;
        table.clearSelection();
    }

    private void search() {
        String keyword = txtTK.getText();
        if (!keyword.isEmpty()) {
            try {
                fillTable(khachHangServices.selectByKeyword(keyword));
            } catch (SQLException e) {
                MsgBox.alert(null, e.getMessage());
            }
        }
        else {
            fillTable(khachHangServices.findAll());
        }
        clearForm();
    }

    private void createUIComponents() {
        panelKhachHangQL = this;
    }
}
