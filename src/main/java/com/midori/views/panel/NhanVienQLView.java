package com.midori.views.panel;

import com.formdev.flatlaf.FlatClientProperties;
import com.midori.models.NhanVien;
import com.midori.services.INhanVienServices;
import com.midori.services.Impl.NhanVienServices;
import com.midori.utils.Auth;
import com.midori.utils.Img;
import com.midori.utils.MsgBox;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public class NhanVienQLView extends JPanel {
    private JTextField txtMa;
    private JTextField txtUsername;
    private JPasswordField txtPass;
    private JRadioButton rdoQuanLy;
    private JRadioButton rdoNhanVien;
    private JTextField txtSdt;
    private JTextField txtEmail;
    private JTextField txtTen;
    private JRadioButton rdoDangLam;
    private JRadioButton rdoDaNghi;
    private JTextField txtTK;
    private JButton btnThem;
    private JButton btnSua;
    private JButton btnXoa;
    private JButton btnReset;
    private JPanel panelNhanVienQL;
    private org.jdesktop.swingx.JXTable table;
    private JPanel a;
    private JButton btnShowHide;
    private JPanel panelCrud;
    private JPanel panelAnh;
    private JButton btnAddImage;
    private JPanel panelAddAnh;
    private DefaultTableModel tableModel;
    private INhanVienServices nhanVienServices;
    private int index = -1;
    private boolean check = Auth.isManager();
    private String image;

    public NhanVienQLView() {
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            index = table.getSelectedRow();
            if (index >= 0) showItem(index);
            }
        });

        btnShowHide.setIcon(Img.readSVG("show.svg"));
        btnShowHide.addActionListener((e) -> {
            if (check) {
                txtPass.setEchoChar((char) 0); //By this line of code. We will actually see the actual characters
                btnShowHide.setIcon(Img.readSVG("show.svg"));
                check = false;
            } else {//•
                txtPass.setEchoChar('•');
                btnShowHide.setIcon(Img.readSVG("hide.svg"));
                check = true;
            }
        });

        btnThem.addActionListener((e) -> insert());
        btnSua.addActionListener((e) -> update());
        btnXoa.addActionListener((e) -> delete());
        btnReset.addActionListener((e) -> clearForm());
        txtTK.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                search();
            }
        });
        btnAddImage.addActionListener((e) -> image = Img.openImageFile(panelAnh));
        initComponent();
    }

    private void initComponent() {
        nhanVienServices = new NhanVienServices();

        if (check) {
            a.setVisible(true);
            panelCrud.setVisible(true);
            panelAddAnh.setVisible(true);
        } else {
            a.setVisible(false);
            panelCrud.setVisible(false);
            panelAddAnh.setVisible(false);
        }

        txtTK.putClientProperty(
                FlatClientProperties.PLACEHOLDER_TEXT,
                "Nhập mã hoặc tên nhân viên...");
        txtTK.putClientProperty(
                FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON,
                true);

        tableModel = (DefaultTableModel) table.getModel();
        tableModel.setColumnIdentifiers(new String[]{"Mã nhân viên", "Username", "Tên nhân viên", "Ảnh", "Vai trò", "Số điện thoại", "Email", "Ngày tạo", "Trạng thái"});
        fillTable(nhanVienServices.findAll());

        if (tableModel.getRowCount() > 0) {
            showItem(0);
            table.setRowSelectionInterval(0, 0);
        }
    }

    public void fillTable(List<NhanVien> list) {
        tableModel.setRowCount(0);
        for (NhanVien nhanVien : list) {
            tableModel.addRow(nhanVien.toObject());
        }
        table.setDefaultEditor(Object.class, null);
        table.packAll();
    }

    private void showItem(int index) {
        try {
            int id = (int) tableModel.getValueAt(index, 0);
            NhanVien nv = nhanVienServices.findById(id);
            txtMa.setText(String.valueOf(nv.getId()));
            txtUsername.setText(String.valueOf(nv.getUsername()));
            txtPass.setText(String.valueOf(nv.getPassword()));
            txtTen.setText(String.valueOf(nv.getTenNhanVien()));
            txtSdt.setText(String.valueOf(nv.getSdt()));
            txtEmail.setText(String.valueOf(nv.getEmail()));
            if (nv.isVaiTro()) rdoQuanLy.setSelected(true);
            else rdoNhanVien.setSelected(true);
            if (nv.isTrangThai()) rdoDangLam.setSelected(true);
            else rdoDaNghi.setSelected(true);
            image = "images/" + nv.getAnh();
            panelAnh.removeAll();
            panelAnh.add(Img.getCircleImage(nv.getAnh(), 300, 0, null, 0));
            panelAnh.revalidate();
        } catch (Exception e) {
            MsgBox.alert(null, e.getMessage());
        }
    }

    private void clearForm() {
        txtEmail.setText("");
        txtMa.setText("");
        txtPass.setText("");
        txtSdt.setText("");
        txtTen.setText("");
        txtUsername.setText("");
        rdoQuanLy.setSelected(true);
        rdoDangLam.setSelected(true);
        image = "images/no-image.png";
        panelAnh.removeAll();
        panelAnh.add(Img.getCircleImage("no-image.png", 300, 0, null, 0));
        panelAnh.revalidate();
        index = -1;
        table.clearSelection();
    }

    public NhanVien readForm() {
        String username, password, ten, hinhAnh, sdt, email;
        boolean vaiTro, trangThai;

        username = txtUsername.getText().trim();
        if (username.isEmpty()) {
            MsgBox.alert(null, "Username không được để trống");
            return null;
        }
        if (username.matches(".*[^a-zA-Z0-9_].*")) {
            MsgBox.alert(null, "Username không được chứa ký tự đặc biệt");
            return null;
        }

        password = new String(txtPass.getPassword());
        if (password.trim().isEmpty()) {
            MsgBox.alert(null, "Password không được để trống");
            return null;
        }
        if (password.contains(" ")) {
            MsgBox.alert(null, "Password không được chứa khoảng trắng");
            return null;
        }

        ten = txtTen.getText().trim();
        if (ten.isEmpty()) {
            MsgBox.alert(null, "Không được để trống tên, vui lòng nhập tên");
            return null;
        }
        if (ten.matches(".*\\d.*") || ten.matches(".*[^\\p{L}0-9_\\s].*")) {
            MsgBox.alert(null, "Tên không được chứa số hoặc kí tự đặc biệt");
            return null;
        }

        sdt = txtSdt.getText().trim();
        if (!sdt.isEmpty() && !sdt.matches("^0\\d{9}$")) {
            MsgBox.alert(null, "Số điện thoại phải bắt đầu bằng số 0 và có tổng cộng 10 số");
            return null;
        }

        email = txtEmail.getText().trim();
        if (email.isEmpty()) {
            MsgBox.alert(null, "Email không được để trống");
            return null;
        }
        if (!email.matches("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$")) {
            MsgBox.alert(null, "Email không hợp lệ");
            return null;
        }

        vaiTro = rdoQuanLy.isSelected() ? true : false;
        trangThai = rdoDangLam.isSelected() ? true : false;

        hinhAnh = image;

        return new NhanVien(username, password, vaiTro, ten, hinhAnh, sdt, email, new Timestamp(System.currentTimeMillis()), trangThai);
    }

    private void insert() {
        NhanVien nv = readForm();
        if (nv != null) {
            try {
                MsgBox.alert(null, nhanVienServices.save(nv));
                fillTable(nhanVienServices.findAll());
                clearForm();
            } catch (Exception e) {
                MsgBox.alert(null, e.getMessage());
            }
        }
    }

    private void update() {
        if (MsgBox.confirm(null, "Bạn có muốn sửa không?")) {
            NhanVien nhanVien = readForm();
            if (nhanVien != null && index >= 0) {
                try {
                    nhanVien.setId((int) table.getValueAt(index, 0));
                    MsgBox.alert(null, nhanVienServices.update(nhanVien));
                    fillTable(nhanVienServices.findAll());
                    clearForm();
                } catch (Exception e) {
                    MsgBox.alert(null, e.getMessage());
                }
            }
        }
    }

    private void delete() {
        if (MsgBox.confirm(null, "Bạn có muốn xoá không?")) {
            int id = (int) table.getValueAt(index, 0);
            try {
                MsgBox.alert(null, nhanVienServices.deleteById(id));
                fillTable(nhanVienServices.findAll());
                clearForm();
            } catch (Exception e) {
                MsgBox.alert(null, e.getMessage());
            }
        }
    }

    private void search() {
        String keyword = txtTK.getText();
        if (!keyword.isEmpty()) {
            try {
                fillTable(nhanVienServices.selectByKeyword(keyword));
            } catch (SQLException e) {
                MsgBox.alert(null, e.getMessage());
            }
        }
        else {
            fillTable(nhanVienServices.findAll());
        }
        clearForm();
    }

    private void createUIComponents() {
        panelNhanVienQL = this;
    }
}
