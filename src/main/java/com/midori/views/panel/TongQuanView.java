package com.midori.views.panel;

import com.midori.views.libraries.chart.CurveLineChart;
import com.midori.views.libraries.chart.ModelChart;

import javax.swing.*;
import java.awt.*;

public class TongQuanView extends CurveLineChart {
    private JPanel chart;
    private JPanel tongQuanView;

    public TongQuanView() {
        chart.add(this);
        setSize(500, 500);
        setTitle("Chart Data");
        addLegend("Amount", Color.decode("#7b4397"), Color.decode("#dc2430"));
        addLegend("Cost", Color.decode("#e65c00"), Color.decode("#F9D423"));
        addLegend("Profit", Color.decode("#0099F7"), Color.decode("#F11712"));
        test();
    }

    private void test() {
        clear();
        addData(new ModelChart("January", new double[]{500, 50,  100}));
        addData(new ModelChart("February", new double[]{600, 300, 150}));
        addData(new ModelChart("March", new double[]{200, 50, 900}));
        addData(new ModelChart("April", new double[]{480, 700, 100}));
        addData(new ModelChart("May", new double[]{350, 540, 500}));
        addData(new ModelChart("June", new double[]{450, 800, 100}));
        addData(new ModelChart("July", new double[]{450, 800, 100}));
        addData(new ModelChart("August", new double[]{450, 800, 100}));
        addData(new ModelChart("September", new double[]{450, 800, 100}));
        addData(new ModelChart("October", new double[]{450, 800, 100}));
        addData(new ModelChart("November", new double[]{450, 800, 100}));
        addData(new ModelChart("December", new double[]{450, 800, 100}));
        start();
    }
}
