package com.midori.views.panel;

import com.formdev.flatlaf.FlatClientProperties;
import com.midori.models.Giay;
import com.midori.models.GiayChiTiet;
import com.midori.services.*;
import com.midori.services.Impl.GiayChiTietServices;
import com.midori.services.Impl.GiayServices;
import com.midori.utils.Auth;
import com.midori.utils.CurrencyConverter;
import com.midori.utils.Img;
import com.midori.utils.MsgBox;
import com.midori.views.components.menu.MenuModel;
import lombok.Setter;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class SanPhamChiTietQL extends JPanel {
    private SanPhamQLView sanPhamQLView;
    private JTextField txtTK;
    private JButton btnThem;
    private JButton btnSua;
    private JButton btnXoa;
    private JButton btnReset;
    private JXTable table;
    private JTextField txtMaGiayCT;
    private JFormattedTextField txtGiaGoc;
    private JTextField txtSoLuong;
    private JSpinner listKichCo;
    private JComboBox cboDeGiay;
    private JXDatePicker txtNgayThem;
    private JRadioButton rdoConHang;
    private JRadioButton rdoHetHang;
    private JList listMauSac;
    private JComboBox cboTenGiay;
    private JButton btnAddImage;
    private JPanel panelSanPhamChiTietQL;
    private JButton btnQuanLyGiay;
    private JPanel panelAnh;
    private DefaultListModel<MenuModel> listModel;
    private DefaultTableModel tableModel;
    private DefaultComboBoxModel<Giay> comboBoxModel;
    private IGiayChiTietServices sanPhamChiTietQL;
    private IGiayServices giayServices;
    private int index = -1;
    private String image;

    public SanPhamChiTietQL() {
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            index = table.getSelectedRow();
            if (index >= 0) showItem(index);
            }
        });
        btnThem.addActionListener((e) -> insert());
        btnSua.addActionListener((e) -> update());
        btnXoa.addActionListener((e) -> delete());
        btnReset.addActionListener((e) -> clearForm());
        btnAddImage.addActionListener((e) -> image = Img.openImageFile(panelAnh));
        btnQuanLyGiay.addActionListener((e) -> xemSP());
        txtTK.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                search();
            }
        });
        initComponent();
    }

    private void initComponent() {
        giayServices = new GiayServices();
        sanPhamChiTietQL = new GiayChiTietServices();

        txtTK.putClientProperty(
                FlatClientProperties.PLACEHOLDER_TEXT,
                "Nhập mã hoặc tên giày...");
        txtTK.putClientProperty(
                FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON,
                true);
        listKichCo.setModel(new SpinnerNumberModel(38, 35, 44, 1));

        fillComboboxGiay();

        tableModel = (DefaultTableModel) table.getModel();
        tableModel.setColumnIdentifiers(new String[]{"Mã giày CT", "Tên giày", "Ảnh", "Màu sắc", "Giá gốc", "Số lượng", "Kích cỡ", "Đế giày", "Ngày thêm", "Ngày sửa", "Trạng thái"});
        fillTable(sanPhamChiTietQL.findAll());

        txtGiaGoc.setFormatterFactory(CurrencyConverter.getVnCurrency());

        if (tableModel.getRowCount() > 0) {
            showItem(0);
            table.setRowSelectionInterval(0, 0);
        }
    }

    private void fillComboboxGiay() {
        try {
            comboBoxModel = new DefaultComboBoxModel<>();
            cboTenGiay.setModel(comboBoxModel);
            for (Giay giay : giayServices.findAll()) {
                comboBoxModel.addElement(giay);
            }
        } catch (Exception e) {
            MsgBox.alert(null, e.getMessage());
        }
    }

    public void fillTable(List<GiayChiTiet> list) {
        tableModel.setRowCount(0);
        for (GiayChiTiet giayChiTiet : list) {
            tableModel.addRow(giayChiTiet.toObject());
        }
        table.setDefaultEditor(Object.class, null);
        table.packAll();
    }

    private void showItem(int index) {
        try {
            int id = (int) table.getValueAt(index, 0);
            GiayChiTiet gct = sanPhamChiTietQL.findById(id);
            cboTenGiay.setSelectedItem(gct.getGiay());
            txtMaGiayCT.setText(String.valueOf(gct.getIdGiayCT()));
            txtGiaGoc.setText(String.valueOf(gct.getGiaGoc()));
            txtSoLuong.setText(String.valueOf(gct.getSoLuong()));
            List<String> mauSac = new ArrayList<>();
            for (int i = 0; i < listMauSac.getModel().getSize(); i++)
                mauSac.add(listMauSac.getModel().getElementAt(i).toString());
            gct.getMauSac();
            List<Integer> idexs = new ArrayList<>();
            for (int i = 0; i < mauSac.size(); i++) {
                for (int j = 0; j < gct.getMauSac().size(); j++) {
                    if (mauSac.get(i).equals(gct.getMauSac().get(j))) {
                        idexs.add(i);
                        break;
                    }
                }
            }
            listMauSac.setSelectedIndices(idexs.stream()
                    .mapToInt(Integer::intValue)
                    .toArray());
            cboDeGiay.setSelectedItem(gct.getDeGiay());
            listKichCo.setValue(gct.getKichCo());
            if (gct.isTrangThai()) rdoConHang.setSelected(true);
            else rdoHetHang.setSelected(true);
            image = "images/" + gct.getAnh();
            panelAnh.removeAll();
            panelAnh.add(Img.getCircleImage(gct.getAnh(), 300, 0, null, 0));
            panelAnh.revalidate();
        } catch (Exception e) {
            MsgBox.alert(null, e.getMessage());
        }
    }

    private void clearForm() {
        txtGiaGoc.setText("");
        txtSoLuong.setText("");
        cboTenGiay.setSelectedIndex(0);
        listMauSac.setSelectedIndex(0);
        listKichCo.getValue();
        cboDeGiay.setSelectedIndex(0);
        txtMaGiayCT.setText("");
        rdoConHang.setSelected(true);
        image = "images/no-image.png";
        panelAnh.removeAll();
        panelAnh.add(Img.getCircleImage("no-image.png", 300, 0, null, 0));
        panelAnh.revalidate();
        index = -1;
        table.clearSelection();
    }

    public GiayChiTiet readForm() {
        String deGiay, hinhAnh;
        long giaGoc;
        int soLuong, kichCo;
        boolean trangThai;
        List<String> mauSac;
        if (listMauSac.getSelectedValuesList().isEmpty()) {
            MsgBox.alert(null, "Vui lòng chọn màu sắc");
            return null;
        } else mauSac = listMauSac.getSelectedValuesList();
        Giay giay = (Giay) cboTenGiay.getSelectedItem();
        if (txtGiaGoc.getText().trim().isEmpty()) {
            MsgBox.alert(null, "Giá gốc không được để trống");
            return null;
        } else {
            try {
                giaGoc = CurrencyConverter.parseLong(txtGiaGoc.getText().trim());
                if (giaGoc <= 0) {
                    MsgBox.alert(null, "Giá gốc phải là số nguyên dương");
                    return null;
                }
            } catch (NumberFormatException e) {
                MsgBox.alert(null, "Giá gốc không đúng định dạng, vui lòng nhập số");
                return null;
            }
        }
        if (txtSoLuong.getText().trim().isEmpty()) {
            MsgBox.alert(null, "Số lượng không được để trống");
            return null;
        } else {
            try {
                soLuong = Integer.parseInt(txtSoLuong.getText().trim());
                if (soLuong <= 0) {
                    MsgBox.alert(null, "Số lượng phải là số nguyên dương");
                    return null;
                }
            } catch (NumberFormatException e) {
                MsgBox.alert(null, "Số lượng không đúng định dạng, vui lòng nhập số");
                return null;
            }
        }
        kichCo = (int) listKichCo.getValue();
        deGiay = cboDeGiay.getSelectedItem().toString();
        trangThai = rdoConHang.isSelected() ? true : false;
        hinhAnh = image;
        return new GiayChiTiet(giay, hinhAnh, mauSac, giaGoc, soLuong, kichCo, deGiay, new Timestamp(System.currentTimeMillis()), new Timestamp(System.currentTimeMillis()), trangThai);
    }

    public void insert() {
        GiayChiTiet giayChiTiet = readForm();
        if (giayChiTiet != null) {
            try {
                MsgBox.alert(null, sanPhamChiTietQL.save(giayChiTiet));
                fillTable(sanPhamChiTietQL.findAll());
                clearForm();
            } catch (Exception e) {
                MsgBox.alert(null, e.getMessage());
            }
        }
    }

    private void update() {
        if (MsgBox.confirm(null, "Bạn có muốn sửa không?")) {
            GiayChiTiet giayChiTiet = readForm();
            if (giayChiTiet != null && index >= 0) {
                try {
                    giayChiTiet.setIdGiayCT((int) table.getValueAt(index, 0));
                    MsgBox.alert(null, sanPhamChiTietQL.update(giayChiTiet));
                    fillTable(sanPhamChiTietQL.findAll());
                    clearForm();
                } catch (Exception e) {
                    MsgBox.alert(null, e.getMessage());
                }
            }
        }
    }

    private void delete() {
        if (MsgBox.confirm(null, "Bạn có muốn xoá không?")) {
            int id = (int) table.getValueAt(index, 0);
            try {
                MsgBox.alert(null, sanPhamChiTietQL.deleteById(id));
                fillTable(sanPhamChiTietQL.findAll());
                clearForm();
            } catch (Exception e) {
                MsgBox.alert(null, e.getMessage());
            }
        }
    }

    private void xemSP() {
        sanPhamQLView = new SanPhamQLView();
        Auth.content.removeAll();
        Auth.content.add(sanPhamQLView);
        Auth.content.revalidate();
    }

    private void search() {
        String keyword = txtTK.getText();
        if (!keyword.isEmpty()) {
            try {
                fillTable(sanPhamChiTietQL.selectByKeyword(keyword));
            } catch (SQLException e) {
                MsgBox.alert(null, e.getMessage());
            }
        }
        else {
            fillTable(sanPhamChiTietQL.findAll());
        }
        clearForm();
    }

    private void createUIComponents() {
        panelSanPhamChiTietQL = this;
    }
}
