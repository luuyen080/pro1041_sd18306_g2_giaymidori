package com.midori.views.panel;

import com.formdev.flatlaf.FlatClientProperties;
import com.midori.models.DonHang;
import com.midori.services.IDonHangServices;
import com.midori.services.Impl.DonHangServices;
import com.midori.utils.CurrencyConverter;
import com.midori.utils.MsgBox;
import com.midori.views.cart.CartModel;
import org.jdesktop.swingx.JXTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.List;

public class DonHangQLView extends JPanel {
    private JTextField txtMaDH;
    private JTextField txtTenKH;
    private JFormattedTextField txtTongTien;
    private JTextField txtMaNV;
    private JXTable table;
    private JPanel panelDonHangQL;
    private JTextField txtTK;
    private JXTable tableDHCT;
    private DefaultTableModel tableModel;
    private DefaultTableModel tableModelDHCT;
    private IDonHangServices donHangServices;
    private int index = -1;

    public DonHangQLView() {
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            index = table.getSelectedRow();
            if (index >= 0) showItem(index);
            }
        });
        txtTK.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {search();
            }
        });
        initComponent();
    }

    private void initComponent() {
        donHangServices = new DonHangServices();

        txtTK.putClientProperty(
                FlatClientProperties.PLACEHOLDER_TEXT,
                "Nhập mã đơn hàng...");
        txtTK.putClientProperty(
                FlatClientProperties.TEXT_FIELD_SHOW_CLEAR_BUTTON,
                true);

        tableModel = (DefaultTableModel) table.getModel();
        tableModel.setColumnIdentifiers(new String[]{"Mã đơn hàng", "Mã nhân viên", "Tên khách hàng", "Tổng tiền", "Ngày tạo"});
        fillTable(donHangServices.findAll());
        tableModelDHCT = (DefaultTableModel) tableDHCT.getModel();
        tableModelDHCT.setColumnIdentifiers(new String[]{"Tên sản phẩm", "Đơn giá", "Số lượng", "Thành tiền"});

        txtTongTien.setFormatterFactory(CurrencyConverter.getVnCurrency());

        if (tableModel.getRowCount() > 0) {
            showItem(0);
            table.setRowSelectionInterval(0, 0);
        }
    }

    public void fillTable(List<DonHang> list) {
        tableModel.setRowCount(0);
        for (DonHang donHang : list) {
            tableModel.addRow(donHang.toObject());
        }
        table.setDefaultEditor(Object.class, null);
        table.packAll();
    }

    public void fillTableDHCT(List<CartModel> list) {
        tableModelDHCT.setRowCount(0);
        for (CartModel cartModel : list) {
            tableModelDHCT.addRow(new Object[] {
                    cartModel.getGiayChiTiet().getGiay().getTenGiay(),
                    CurrencyConverter.parseString(cartModel.getGiayChiTiet().getGiaGoc()),
                    cartModel.getSoLuong().getValue(),
                    CurrencyConverter.parseString(cartModel.getGiayChiTiet().getGiaGoc() * (int) cartModel.getSoLuong().getValue())
            });
        }
        tableDHCT.setDefaultEditor(Object.class, null);
        tableDHCT.packAll();
    }

    private void showItem(int index) {
        int id = (int) tableModel.getValueAt(index, 0);
        DonHang donHang = donHangServices.findById(id);
        txtMaDH.setText(String.valueOf(donHang.getId()));
        txtMaNV.setText(String.valueOf(donHang.getNv().getId()));
        txtTenKH.setText(donHang.getKh().getTenKhachHang());
        txtTongTien.setText(String.valueOf(donHang.getTongTien()));
        fillTableDHCT(donHang.getGioHangModels());
    }

    private void clearForm() {
        txtMaDH.setText("");
        txtMaNV.setText("");
        txtTenKH.setText("");
        txtTongTien.setText("");
        index = -1;
        table.clearSelection();
    }

    private void search() {
        String keyword = txtTK.getText();
        if (!keyword.isEmpty()) {
            try {
                fillTable(donHangServices.selectByKeyword(keyword));
            } catch (SQLException e) {
                MsgBox.alert(null, e.getMessage());
            }
        }
        else {
            fillTable(donHangServices.findAll());
        }
        clearForm();
    }

    private void createUIComponents() {
        panelDonHangQL = this;
    }
}
