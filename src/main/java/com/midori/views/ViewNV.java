package com.midori.views;

import com.formdev.flatlaf.FlatClientProperties;
import com.midori.Application;
import com.midori.models.Giay;
import com.midori.services.IDonHangServices;
import com.midori.services.IGiayChiTietServices;
import com.midori.services.IGiayServices;
import com.midori.services.IKhachHangServices;
import com.midori.services.INhanVienServices;
import com.midori.services.Impl.DonHangServices;
import com.midori.services.Impl.GiayChiTietServices;
import com.midori.services.Impl.KhachHangServices;
import com.midori.services.Impl.NhanVienServices;
import com.midori.utils.Auth;
import com.midori.utils.Img;
import com.midori.utils.MsgBox;
import com.midori.views.components.menu.CustomMenu;
import com.midori.views.components.menu.MenuModel;
import com.midori.views.panel.DonHangNVView;
import com.midori.views.panel.KhachHangNVView;
import com.midori.views.panel.NhanVienQLView;
import com.midori.views.panel.SPCT_NVView;
import com.midori.views.panel.SanPhamChiTietQL;
import com.midori.views.panel.SanPhamQLView;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class ViewNV extends JPanel {
    private JList listMenu;
    private JButton btnDangXuat;
    private JLabel lblUsername;
    private JPanel cardPanel;
    private KhachHangNVView khachHangNV;
    private NhanVienQLView nhanVienQL;
    private SanPhamChiTietQL sanPhamChiTietQL;
    private SanPhamQLView sanPhamQL;
    private DonHangNVView donHangNV;
    private JPanel panelNhanVien;
    private JPanel content;
    private JLabel lblLogo;
    private DefaultListModel<MenuModel> listModel;
    private DefaultTableModel tableModel;
    private DefaultComboBoxModel<Giay> comboBoxModel;
    private IGiayChiTietServices serviceGiayChiTiet;
    private IGiayServices serviceGiay;
    private INhanVienServices nhanVienServices;
    private IKhachHangServices khachHangServices;
    private IDonHangServices donHangServices;
    private SPCT_NVView spctNvView;
    private KhachHangNVView khachHangNVView;
    private NhanVienQLView nhanVienQLView;
    private DonHangNVView donHangNVView;

    public ViewNV(Application application) {
        initComponent();

        listMenu.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                chooseItemMenu();
            }
        });
        btnDangXuat.addActionListener((e) -> logout(application));
    }

    public void initComponent() {
        lblLogo.setIcon(new ImageIcon(Img.readImage("logo.png").getScaledInstance(150, 150, Image.SCALE_SMOOTH)));

        serviceGiayChiTiet = new GiayChiTietServices();
        nhanVienServices = new NhanVienServices();
        khachHangServices = new KhachHangServices();
        donHangServices = new DonHangServices();

        lblUsername.setText("Xin chào, " + Auth.user.getTenNhanVien());
        listModel = new DefaultListModel<>();
        listMenu.setModel(listModel);

        fillList(List.of(
                new MenuModel("sanpham-w.svg", "sanpham-b.svg", "Bán hàng"),
                new MenuModel("khachhang-w.svg", "khachhang-b.svg", "Khách hàng"),
                new MenuModel("nhanvien-w.svg", "nhanvien-b.svg", "Nhân viên"),
                new MenuModel("donhang-w.svg", "donhang-b.svg", "Đơn hàng")
        ));
        listMenu.setSelectedIndex(0);

        btnDangXuat.setIcon(Img.readSVG("dangxuat-w.svg"));
        btnDangXuat.setIconTextGap(25);
        btnDangXuat.setHorizontalAlignment(SwingConstants.LEFT);
        btnDangXuat.putClientProperty(FlatClientProperties.STYLE, "arc:0");
    }

    public void fillList(List<MenuModel> list) {
        listModel.clear();
        for (MenuModel menuModel : list) listModel.addElement(menuModel);
    }

    private void chooseItemMenu() {
        content.removeAll();
        switch (listMenu.getSelectedIndex()) {
            case 0 -> {
                spctNvView = new SPCT_NVView();
                content.add(spctNvView);
            }
            case 1 -> {
                khachHangNVView = new KhachHangNVView();
                content.add(khachHangNVView);
            }
            case 2 -> {
                nhanVienQLView = new NhanVienQLView();
                content.add(nhanVienQLView);
            }
            case 3 -> {
                donHangNVView = new DonHangNVView();
                content.add(donHangNVView);
            }
        }
        content.revalidate();
    }

    private void logout(Application application) {
        if (MsgBox.confirm(null, "Bạn có muốn đăng xuất không?")) {
            Auth.clear();
            application.setContentPane(new ViewDangNhap(application));
            application.pack();
            application.setLocationRelativeTo(null);
        }
    }

    private void createUIComponents() {
        panelNhanVien = this;
        listMenu = new CustomMenu();
    }
}
