package com.midori.views.libraries;

public enum ShadowType {
    CENTER, TOP_RIGHT, TOP_LEFT, BOT_RIGHT, BOT_LEFT, BOT, TOP
}