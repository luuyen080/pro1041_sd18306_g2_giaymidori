package com.midori.views;

import com.midori.Application;
import com.midori.models.NhanVien;
import com.midori.services.INhanVienServices;
import com.midori.services.Impl.NhanVienServices;
import com.midori.utils.Auth;
import com.midori.utils.MsgBox;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

public class ViewDangNhap extends JPanel {
    private Application application;
    private JPanel panelDangNhap;
    private JTextField txtTaiKhoan;
    private JPasswordField txtMatKhau;
    private JButton btnDangNhap;
    private JLabel lblQuenMatKhau;
    private INhanVienServices nhanVienServices;

    public ViewDangNhap(Application application) {
        this.application = application;
        nhanVienServices = new NhanVienServices();
//        txtMatKhau.setEchoChar('ฅ');

        btnDangNhap.addActionListener((e) -> login());
        lblQuenMatKhau.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                openQuenMatKhau();
            }
        });
    }

    private void login() {
        try {
            String username = txtTaiKhoan.getText();
            String password = new String(txtMatKhau.getPassword());
            NhanVien nhanVien = nhanVienServices.findByUsername(username);

            if (!nhanVien.getPassword().equals(password)) MsgBox.alert(null, "Sai tên đăng nhập hoặc mật khẩu!");
            else if (!nhanVien.isTrangThai()) MsgBox.alert(null, "Bạn không có quyền đăng nhập vào phần mềm!!");
            else {
                Auth.user = nhanVien;
                if (Auth.isManager()) application.setContentPane(new ViewQL(application));
                else application.setContentPane(new ViewNV(application));
                application.setSize(Toolkit.getDefaultToolkit().getScreenSize().width, Toolkit.getDefaultToolkit().getScreenSize().height);
                application.setLocationRelativeTo(null);
            }
        } catch (SQLException e) {
            MsgBox.alert(null, e.getMessage());
        }
    }

    private void openQuenMatKhau() {
        ViewQuenMatKhau quenMatKhauDialog = new ViewQuenMatKhau();
        quenMatKhauDialog.setVisible(true);
    }

    private void createUIComponents() {
        panelDangNhap = this;
    }
}
