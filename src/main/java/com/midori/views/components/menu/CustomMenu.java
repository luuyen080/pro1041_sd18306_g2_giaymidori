package com.midori.views.components.menu;

import com.formdev.flatlaf.FlatClientProperties;
import com.midori.utils.Img;

import javax.swing.*;
import java.awt.*;

public class CustomMenu extends JList {
    public CustomMenu() {
        setCellRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                JButton btn = new JButton(value.toString());
                MenuModel menuModel = (MenuModel) value;
                btn.setText(menuModel.getName());
                btn.setIcon(Img.readSVG(menuModel.getIcon()));
                btn.setIconTextGap(25);
                btn.setBackground(Color.decode("#E76941"));
                btn.setForeground(Color.decode("#F2F2F2"));
                btn.setHorizontalAlignment(SwingConstants.LEFT);
                btn.setBorderPainted(false);
                btn.putClientProperty(FlatClientProperties.STYLE, "arc:0");
                if (isSelected) {
                    btn.setIcon(Img.readSVG(menuModel.getIconContrast()));
                    btn.setBackground(Color.decode("#F2F2F2"));
                    btn.setForeground(Color.decode("#787172"));
                }
                return btn;
            }
        });
    }
}
