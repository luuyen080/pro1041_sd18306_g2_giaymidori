package com.midori.views.components.menu;

public class MenuModel {
    private String icon;
    private String iconContrast;
    private String name;

    public MenuModel() {
    }

    public MenuModel(String icon, String iconContrast, String name) {
        this.icon = icon;
        this.iconContrast = iconContrast;
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIconContrast() {
        return iconContrast;
    }

    public void setIconContrast(String iconContrast) {
        this.iconContrast = iconContrast;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
