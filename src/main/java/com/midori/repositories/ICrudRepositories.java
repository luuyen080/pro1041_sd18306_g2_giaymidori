package com.midori.repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface ICrudRepositories<T, K> {
    public List<T> findAll() ;
    public T findById(K id);
    public void deleteById(K id) throws SQLException;
    public void update(T t) throws SQLException;
    public int save(T t) throws SQLException;
    public List<T> queryBySql(ResultSet rs) throws SQLException;
}
