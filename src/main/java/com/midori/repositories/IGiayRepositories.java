package com.midori.repositories;

import com.midori.models.Giay;
import com.midori.models.GiayChiTiet;

import java.sql.SQLException;
import java.util.List;

public interface IGiayRepositories extends ICrudRepositories<Giay, Integer> {
    public List<Giay> selectByKeyword(String keyword) throws SQLException;
}
