package com.midori.repositories.Impl;

import com.midori.models.NhanVien;
import com.midori.repositories.INhanVienRepositories;
import com.midori.configs.JdbcHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NhanVienRepositories implements INhanVienRepositories {
    String insert_sql = "insert into TaiKhoan (username, password, vaiTro, tenNhanVien, anh, sdt, email, ngayTao, trangThai)\n" +
            "values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    String update_sql = "update TaiKhoan\n" +
            "set [username] = ?, [password] = ?, vaiTro = ?, tenNhanVien = ?, anh = ?, sdt = ?, email = ?, ngayTao = ?, trangThai = ?\n" +
            "where idTaiKhoan = ?";
    String delete_sql = "delete from TaiKhoan\n" +
            "where idTaiKhoan = ?";
    String select_all_sql = "select * from TaiKhoan";
    String select_by_id_sql = "select * from TaiKhoan\n" +
            "where idTaiKhoan = ?";
    String select_by_username_sql = "select * from TaiKhoan\n" +
            "where username = ?";
    String select_by_email_sql = "select * from TaiKhoan\n" +
            "where email = ?";

    @Override
    public List<NhanVien> findAll() {
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = connection.prepareStatement(select_all_sql);
            return queryBySql(stmt.executeQuery());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public NhanVien findById(Integer id) {
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = null;
            stmt = connection.prepareStatement(select_by_id_sql);
            JdbcHelper.setPrepareStatement(stmt, id);
            return queryBySql(stmt.executeQuery()).get(0);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteById(Integer id) throws SQLException {
        Connection connection = JdbcHelper.getConnection();
        connection.setAutoCommit(false);
        PreparedStatement stmt = connection.prepareStatement(delete_sql, PreparedStatement.RETURN_GENERATED_KEYS);
        JdbcHelper.setPrepareStatement(stmt, id);
        stmt.executeUpdate();
        connection.commit();
    }

    @Override
    public void update(NhanVien nhanVien) throws SQLException {
        Connection connection = JdbcHelper.getConnection();
        connection.setAutoCommit(false);
        PreparedStatement stmt = connection.prepareStatement(update_sql, PreparedStatement.RETURN_GENERATED_KEYS);
        JdbcHelper.setPrepareStatement(stmt, nhanVien.getUsername(), nhanVien.getPassword(), nhanVien.isVaiTro(), nhanVien.getTenNhanVien(), nhanVien.getAnh(), nhanVien.getSdt(), nhanVien.getEmail(), nhanVien.getNgayTao(), nhanVien.isTrangThai(), nhanVien.getId());
        stmt.executeUpdate();
        connection.commit();
    }

    @Override
    public int save(NhanVien nhanVien) throws SQLException {
        Connection connection = JdbcHelper.getConnection();
        connection.setAutoCommit(false);
        PreparedStatement stmt = connection.prepareStatement(insert_sql, PreparedStatement.RETURN_GENERATED_KEYS);
        JdbcHelper.setPrepareStatement(stmt, nhanVien.getUsername(), nhanVien.getPassword(), nhanVien.isVaiTro(), nhanVien.getTenNhanVien(), nhanVien.getAnh(), nhanVien.getSdt(), nhanVien.getEmail(), nhanVien.getNgayTao(), nhanVien.isTrangThai());
        stmt.executeUpdate();
        connection.commit();
        return JdbcHelper.getGeneratedKey(stmt.getGeneratedKeys());
    }

    @Override
    public List<NhanVien> queryBySql(ResultSet rs) throws SQLException {
        List<NhanVien> list = new ArrayList<>();
        while (rs.next()) {
            NhanVien nhanVien = new NhanVien();
            nhanVien.setId(rs.getInt("idTaiKhoan"));
            nhanVien.setUsername(rs.getString("username"));
            nhanVien.setPassword(rs.getString("password"));
            nhanVien.setVaiTro(rs.getBoolean("vaiTro"));
            nhanVien.setTenNhanVien(rs.getString("tenNhanVien"));
            nhanVien.setAnh(rs.getString("anh"));
            nhanVien.setSdt(rs.getString("sdt"));
            nhanVien.setEmail(rs.getString("email"));
            nhanVien.setNgayTao(rs.getTimestamp("ngayTao"));
            nhanVien.setTrangThai(rs.getBoolean("trangThai"));
            list.add(nhanVien);
        }
        return list;
    }

    @Override
    public NhanVien findByUsername(String username) throws SQLException {
        Connection connection = JdbcHelper.getConnection();
        PreparedStatement stmt = connection.prepareStatement(select_by_username_sql);
        JdbcHelper.setPrepareStatement(stmt, username);
        return queryBySql(stmt.executeQuery()).get(0);
    }

    @Override
    public NhanVien findByEmail(String email) throws SQLException {
        Connection connection = JdbcHelper.getConnection();
        PreparedStatement stmt = connection.prepareStatement(select_by_email_sql);
        JdbcHelper.setPrepareStatement(stmt, email);
        return queryBySql(stmt.executeQuery()).get(0);
    }

    @Override
    public List<NhanVien> selectByKeyword(String keyword) throws SQLException {
        String sql = "select * from TaiKhoan tk \n" +
                "where idTaiKhoan like ? or tenNhanVien like ?";
        Connection connection = JdbcHelper.getConnection();
        PreparedStatement stmt = connection.prepareStatement(sql);
        JdbcHelper.setPrepareStatement(stmt, "%" + keyword + "%", "%" + keyword + "%");
        return queryBySql(stmt.executeQuery());
    }
}
