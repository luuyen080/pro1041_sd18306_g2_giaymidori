package com.midori.repositories.Impl;

import com.midori.models.DonHang;
import com.midori.models.GiayChiTiet;
import com.midori.models.KhachHang;
import com.midori.models.NhanVien;
import com.midori.repositories.IDonHangRepositories;
import com.midori.configs.JdbcHelper;
import com.midori.views.cart.CartModel;
import com.midori.views.cart.CustomSpinner;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DonHangRepositories implements IDonHangRepositories {
    String insert_sql = "insert into DonHang (idKhachHang, idTaiKhoan, tongTien, ngayTao)\n" +
            "values (?, ?, ?, ?)";
    String update_sql = "";
    String delete_sql = "delete from DonHang\n" +
            "where idHoaDon = ?";
    String select_all_sql = "select * from DonHang dh";
    String select_by_id_sql = "select * from DonHang dh\n" +
            "where dh.idHoaDon = ?";
    String insert_dhct_sql = "insert into DonHangChiTiet (idGiayChiTiet, idHoaDon, giaBan, soLuong)\n" +
            "values (?, ?, ?, ?)";

    @Override
    public List<DonHang> findAll() {
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = connection.prepareStatement(select_all_sql);
            return queryBySql(stmt.executeQuery());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public DonHang findById(Integer id) {
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = null;
            stmt = connection.prepareStatement(select_by_id_sql);
            JdbcHelper.setPrepareStatement(stmt, id);
            return queryBySql(stmt.executeQuery()).get(0);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteById(Integer id) throws SQLException {
        Connection connection = JdbcHelper.getConnection();
        connection.setAutoCommit(false);
        PreparedStatement stmt = connection.prepareStatement(delete_sql, PreparedStatement.RETURN_GENERATED_KEYS);
        JdbcHelper.setPrepareStatement(stmt, id);
        stmt.executeUpdate();
        connection.commit();
    }

    @Override
    public void update(DonHang donHang) throws SQLException {

    }

    @Override
    public int save(DonHang donHang) throws SQLException {
        Connection connection = JdbcHelper.getConnection();
        connection.setAutoCommit(false);
        PreparedStatement stmt = connection.prepareStatement(insert_sql, PreparedStatement.RETURN_GENERATED_KEYS);
        JdbcHelper.setPrepareStatement(stmt, donHang.getKh().getIdKhachHang(), donHang.getNv().getId(), donHang.getTongTien(), donHang.getNgayThem());
        stmt.executeUpdate();
        int id = JdbcHelper.getGeneratedKey(stmt.getGeneratedKeys());
        donHang.setId(id);
        themDHCT(id, donHang.getGioHangModels());
        connection.commit();
        return JdbcHelper.getGeneratedKey(stmt.getGeneratedKeys());
    }

    @Override
    public List<DonHang> queryBySql(ResultSet rs) throws SQLException {
        List<DonHang> list = new ArrayList<>();
        while (rs.next()) {
            DonHang donHang = new DonHang();
            KhachHang khachHang = new KhachHang();
            NhanVien nhanVien = new NhanVien();

            khachHang.setIdKhachHang(rs.getInt("idKhachHang"));

            nhanVien.setId(rs.getInt("idTaiKhoan"));

            donHang.setId(rs.getInt("idHoaDon"));
            donHang.setTongTien(rs.getLong("tongTien"));
            donHang.setNgayThem(rs.getTimestamp("ngayTao"));
            donHang.setKh(khachHang);
            donHang.setNv(nhanVien);
            list.add(donHang);
        }
        return list;
    }

    @Override
    public List<CartModel> findDhctByIdDH(int id) {
        String sql = "select * from DonHangChiTiet dhct\n" +
                "where idHoaDon = ?";
        List<CartModel> list = new ArrayList<>();
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = connection.prepareStatement(sql);
            JdbcHelper.setPrepareStatement(stmt, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                GiayChiTiet giayChiTiet = new GiayChiTiet();
                CartModel cartModel = new CartModel(giayChiTiet, null, null);
                CustomSpinner customSpinner = new CustomSpinner();
                customSpinner.setValue(rs.getInt("soLuong"));
                giayChiTiet.setIdGiayCT(rs.getInt("idGiayChiTiet"));
                cartModel.setGiayChiTiet(giayChiTiet);
                cartModel.setGiaBan(rs.getLong("giaBan"));
                cartModel.setSoLuong(customSpinner);
                list.add(cartModel);
            }
            return list;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<DonHang> selectByKeyword(String keyword) throws SQLException {
        String sql = "select * from DonHang dh\n" +
                "where idHoaDon like ?";
        Connection connection = JdbcHelper.getConnection();
        PreparedStatement stmt = connection.prepareStatement(sql);
        JdbcHelper.setPrepareStatement(stmt, "%" + keyword + "%");
        return queryBySql(stmt.executeQuery());
    }

    @Override
    public long findDailyRevenueForDate(int day, int month, int year) {
        String sql = "select sum(dh.tongTien) tong from DonHang dh \n" +
                "where DAY(dh.ngayTao) = ? and MONTH(dh.ngayTao) = ? and YEAR(dh.ngayTao) = ?";
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = null;
            stmt = connection.prepareStatement(sql);
            JdbcHelper.setPrepareStatement(stmt, day, month, year);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) return rs.getInt("tong");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    @Override
    public long findDailyRevenueForMonth(int month, int year) {
        String sql = "select sum(dh.tongTien) tong from DonHang dh \n" +
                "where MONTH(dh.ngayTao) = ? and YEAR(dh.ngayTao) = ?";
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = null;
            stmt = connection.prepareStatement(sql);
            JdbcHelper.setPrepareStatement(stmt, month, year);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) return rs.getInt("tong");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    @Override
    public int findDailySanPhamForDate(int day, int month, int year) {
        String sql = "select sum(dhct.soLuong) soLuong from DonHangChiTiet dhct \n" +
                "join DonHang dh on dh.idHoaDon = dhct.idHoaDon\n" +
                "where DAY(dh.ngayTao) = ? and MONTH(dh.ngayTao) = ? and YEAR(dh.ngayTao) = ?";
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = null;
            stmt = connection.prepareStatement(sql);
            JdbcHelper.setPrepareStatement(stmt, day, month, year);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) return rs.getInt("soLuong");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    @Override
    public int findDailySanPhamForMonth(int month, int year) {
        String sql = "select sum(dhct.soLuong) soLuong from DonHangChiTiet dhct \n" +
                "join DonHang dh on dh.idHoaDon = dhct.idHoaDon\n" +
                "where MONTH(dh.ngayTao) = ? and YEAR(dh.ngayTao) = ?";
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = null;
            stmt = connection.prepareStatement(sql);
            JdbcHelper.setPrepareStatement(stmt, month, year);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) return rs.getInt("soLuong");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    public void themDHCT(int idDonHang, List<CartModel> gioHangModels) {
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = connection.prepareStatement(insert_dhct_sql);
            for (CartModel cartModel : gioHangModels) {
                stmt.setInt(1, cartModel.getGiayChiTiet().getIdGiayCT());
                stmt.setInt(2, idDonHang);
                stmt.setLong(3, cartModel.getGiayChiTiet().getGiaGoc());
                stmt.setInt(4, (int) cartModel.getSoLuong().getValue());
                stmt.executeUpdate();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
