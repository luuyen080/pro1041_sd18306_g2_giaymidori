package com.midori.repositories.Impl;

import com.midori.models.KhachHang;
import com.midori.repositories.IKhachHangRepositories;
import com.midori.configs.JdbcHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class KhachHangRepositories implements IKhachHangRepositories {
    String insert_sql = "insert into KhachHang (tenKhachHang, sdt, diemTichLuy, ngayThem, gioiTinh)\n" +
            "values (?, ?, ?, ?, ?)";
    String update_sql = "update KhachHang\n" +
            "set tenKhachHang = ?, sdt = ?, diemTichLuy = ?, gioiTinh = ?\n" +
            "where idKhachHang = ?";
    String delete_sql = "delete from KhachHang\n" +
            "where idKhachHang = ?";
    String select_all_sql = "select * from KhachHang";
    String select_by_id_sql = "select * from KhachHang\n" +
            "where idKhachHang = ?";

    @Override
    public List<KhachHang> findAll() {
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = connection.prepareStatement(select_all_sql);
            return queryBySql(stmt.executeQuery());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public KhachHang findById(Integer id) {
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = null;
            stmt = connection.prepareStatement(select_by_id_sql);
            JdbcHelper.setPrepareStatement(stmt, id);
            return queryBySql(stmt.executeQuery()).get(0);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteById(Integer id) throws SQLException {
        Connection connection = JdbcHelper.getConnection();
        connection.setAutoCommit(false);
        PreparedStatement stmt = connection.prepareStatement(delete_sql, PreparedStatement.RETURN_GENERATED_KEYS);
        JdbcHelper.setPrepareStatement(stmt, id);
        stmt.executeUpdate();
        connection.commit();
    }

    @Override
    public void update(KhachHang khachHang) throws SQLException {
        Connection connection = JdbcHelper.getConnection();
        connection.setAutoCommit(false);
        PreparedStatement stmt = connection.prepareStatement(update_sql, PreparedStatement.RETURN_GENERATED_KEYS);
        JdbcHelper.setPrepareStatement(stmt, khachHang.getTenKhachHang(), khachHang.getSdt(), khachHang.getDiemTichLuy(), khachHang.isGioiTinh(), khachHang.getIdKhachHang());
        stmt.executeUpdate();
        connection.commit();
    }

    @Override
    public int save(KhachHang khachHang) throws SQLException {
        Connection connection = JdbcHelper.getConnection();
        connection.setAutoCommit(false);
        PreparedStatement stmt = connection.prepareStatement(insert_sql, PreparedStatement.RETURN_GENERATED_KEYS);
        JdbcHelper.setPrepareStatement(stmt, khachHang.getTenKhachHang(), khachHang.getSdt(), khachHang.getDiemTichLuy(), khachHang.getNgayThem(), khachHang.isGioiTinh());
        stmt.executeUpdate();
        connection.commit();
        return JdbcHelper.getGeneratedKey(stmt.getGeneratedKeys());
    }

    @Override
    public List<KhachHang> queryBySql(ResultSet rs) throws SQLException {
        List<KhachHang> list = new ArrayList<>();
        while (rs.next()) {
            KhachHang khachHang = new KhachHang();
            khachHang.setIdKhachHang(rs.getInt("idKhachHang"));
            khachHang.setTenKhachHang(rs.getString("tenKhachHang"));
            khachHang.setSdt(rs.getString("sdt"));
            khachHang.setDiemTichLuy(rs.getInt("diemTichLuy"));
            khachHang.setNgayThem(rs.getTimestamp("ngayThem"));
            khachHang.setGioiTinh(rs.getBoolean("gioiTinh"));
            list.add(khachHang);
        }
        return list;
    }

    @Override
    public List<KhachHang> selectByKeyword(String keyword) throws SQLException {
        String sql = "select * from KhachHang kh \n" +
                "where tenKhachHang like ? or sdt like ?";
        Connection connection = JdbcHelper.getConnection();
        PreparedStatement stmt = connection.prepareStatement(sql);
        JdbcHelper.setPrepareStatement(stmt, "%" + keyword + "%", "%" + keyword + "%");
        return queryBySql(stmt.executeQuery());
    }
}
