package com.midori.repositories.Impl;

import com.midori.models.Giay;
import com.midori.repositories.IGiayRepositories;
import com.midori.configs.JdbcHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GiayReporitories implements IGiayRepositories {
    String insert_sql = "insert into Giay (tenGiay, thuongHieu, ngayThem, ngaySua)\n" +
            "values (?, ?, ?, ?)";
    String update_sql = "update Giay\n" +
            "set tenGiay = ?, thuongHieu = ?, ngaySua = ?\n" +
            "where idGiay = ?";
    String delete_sql = "delete from Giay\n" +
            "where idGiay = ?";
    String select_all_sql = "select * from Giay";
    String select_by_id_sql = "select * from Giay\n" +
            "where idGiay = ?";

    @Override
    public List<Giay> findAll() {
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = connection.prepareStatement(select_all_sql);
            return queryBySql(stmt.executeQuery());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Giay findById(Integer id) {
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = null;
            stmt = connection.prepareStatement(select_by_id_sql);
            JdbcHelper.setPrepareStatement(stmt, id);
            return queryBySql(stmt.executeQuery()).get(0);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteById(Integer id) throws SQLException {
        Connection connection = JdbcHelper.getConnection();
        connection.setAutoCommit(false);
        PreparedStatement stmt = connection.prepareStatement(delete_sql, PreparedStatement.RETURN_GENERATED_KEYS);
        JdbcHelper.setPrepareStatement(stmt, id);
        stmt.executeUpdate();
        connection.commit();
    }

    @Override
    public void update(Giay giay) throws SQLException {
        Connection connection = JdbcHelper.getConnection();
        connection.setAutoCommit(false);
        PreparedStatement stmt = connection.prepareStatement(update_sql, PreparedStatement.RETURN_GENERATED_KEYS);
        JdbcHelper.setPrepareStatement(stmt, giay.getTenGiay(), giay.getThuongHieu(), giay.getNgaySua(), giay.getIdGiay());
        stmt.executeUpdate();
        connection.commit();
    }

    @Override
    public int save(Giay giay) throws SQLException {
        Connection connection = JdbcHelper.getConnection();
        connection.setAutoCommit(false);
        PreparedStatement stmt = connection.prepareStatement(insert_sql, PreparedStatement.RETURN_GENERATED_KEYS);
        JdbcHelper.setPrepareStatement(stmt, giay.getTenGiay(), giay.getThuongHieu(), giay.getNgayThem(), giay.getNgaySua());
        stmt.executeUpdate();
        connection.commit();
        return JdbcHelper.getGeneratedKey(stmt.getGeneratedKeys());
    }

    @Override
    public List<Giay> queryBySql(ResultSet rs) throws SQLException {
        List<Giay> list = new ArrayList<>();
        while (rs.next()) {
            Giay giay = new Giay();
            giay.setIdGiay(rs.getInt("idGiay"));
            giay.setTenGiay(rs.getString("tenGiay"));
            giay.setThuongHieu(rs.getString("thuongHieu"));
            giay.setNgayThem(rs.getTimestamp("ngayThem"));
            giay.setNgaySua(rs.getTimestamp("ngaySua"));
            list.add(giay);
        }
        return list;
    }

    @Override
    public List<Giay> selectByKeyword(String keyword) throws SQLException {
        String sql = "select * from Giay g \n" +
                "where idGiay like ? or tenGiay like ? or thuongHieu like ?";
        Connection connection = JdbcHelper.getConnection();
        PreparedStatement stmt = connection.prepareStatement(sql);
        JdbcHelper.setPrepareStatement(stmt, "%" + keyword + "%", "%" + keyword + "%", "%" + keyword + "%");
        return queryBySql(stmt.executeQuery());
    }
}
