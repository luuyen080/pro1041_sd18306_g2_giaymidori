package com.midori.repositories.Impl;

import com.midori.models.Giay;
import com.midori.models.GiayChiTiet;
import com.midori.repositories.IGiayChiTietRepositories;
import com.midori.configs.JdbcHelper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GiayChiTietRepositories implements IGiayChiTietRepositories {
    String insert_sql = "insert into GiayChiTiet (idGiay, anh, giaGoc, soLuong, kichCo, deGiay, ngayThem, ngaySua, trangThai)\n" +
            "values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    String update_sql = "update GiayChiTiet\n" +
            "set idGiay = ?, anh = ?, giaGoc = ?, soLuong = ?, kichCo = ?, deGiay = ?, ngaySua = ?, trangThai = ?\n" +
            "where idGiayChiTiet = ?";
    String delete_sql = "delete from GiayChiTiet\n" +
            "where idGiayChiTiet = ?";
    String select_all_sql = "select * from GiayChiTiet gct\n" +
            "join Giay g on g.idGiay = gct.idGiay";
    String select_by_id_sql = "select * from GiayChiTiet gct\n" +
            "join Giay g on g.idGiay = gct.idGiay\n" +
            "where idGiayChiTiet = ?";
    String insert_MauSac = "insert into MauSac (idGiayCT, tenMauSac)\n" +
            "values (?, ?)";
    String delete_MauSac = "delete from MauSac\n" +
            "where idGiayCT = ?";
    String select_all_sql_by_trang_thai = "select * from GiayChiTiet gct \n" +
            "join Giay g on g.idGiay = gct.idGiay \n" +
            "where gct.trangThai = 1";
    String select_relative = "select * from GiayChiTiet gct \n" +
            "left join Giay g on g.idGiay = gct.idGiay \n" +
            "left join MauSac ms on ms.idGiayCT = gct.idGiayChiTiet \n" +
            "left join DonHangChiTiet dhct on dhct.idGiayChiTiet = gct.idGiayChiTiet \n" +
            "where gct.idGiayChiTiet = ? and (g.idGiay is not null or ms.idGiayCT is not null or dhct.idGiayChiTiet is not null)";

    @Override
    public List<GiayChiTiet> findAll() {
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = connection.prepareStatement(select_all_sql);
            return queryBySql(stmt.executeQuery());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public GiayChiTiet findById(Integer id) {
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = connection.prepareStatement(select_by_id_sql);
            JdbcHelper.setPrepareStatement(stmt, id);
            return queryBySql(stmt.executeQuery()).get(0);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteById(Integer id) throws SQLException {
        Connection connection = JdbcHelper.getConnection();
        connection.setAutoCommit(false);
        PreparedStatement stmt = connection.prepareStatement(delete_sql, PreparedStatement.RETURN_GENERATED_KEYS);
        JdbcHelper.setPrepareStatement(stmt, id);
        stmt.executeUpdate();
        connection.commit();
    }

    @Override
    public void update(GiayChiTiet giayChiTiet) throws SQLException {
        Connection connection = JdbcHelper.getConnection();
        connection.setAutoCommit(false);
        PreparedStatement stmt = connection.prepareStatement(update_sql, PreparedStatement.RETURN_GENERATED_KEYS);
        JdbcHelper.setPrepareStatement(stmt, giayChiTiet.getGiay().getIdGiay(), giayChiTiet.getAnh(), giayChiTiet.getGiaGoc(), giayChiTiet.getSoLuong(), giayChiTiet.getKichCo(), giayChiTiet.getDeGiay(), giayChiTiet.getNgaySua(), giayChiTiet.isTrangThai(), giayChiTiet.getIdGiayCT());
        deleteMauSac(delete_MauSac, giayChiTiet.getIdGiayCT());
        themMauSac(insert_MauSac, giayChiTiet.getIdGiayCT(), giayChiTiet.getMauSac());
        stmt.executeUpdate();
        connection.commit();
    }

    @Override
    public int save(GiayChiTiet giayChiTiet) throws SQLException {
        Connection connection = JdbcHelper.getConnection();
        connection.setAutoCommit(false);
        PreparedStatement stmt = connection.prepareStatement(insert_sql, PreparedStatement.RETURN_GENERATED_KEYS);
        JdbcHelper.setPrepareStatement(stmt, giayChiTiet.getGiay().getIdGiay(), giayChiTiet.getAnh(), giayChiTiet.getGiaGoc(), giayChiTiet.getSoLuong(), giayChiTiet.getKichCo(), giayChiTiet.getDeGiay(), giayChiTiet.getNgayThem(), giayChiTiet.getNgaySua(), giayChiTiet.isTrangThai());
        stmt.executeUpdate();
        int id = JdbcHelper.getGeneratedKey(stmt.getGeneratedKeys());
        if (giayChiTiet.getMauSac() != null) themMauSac(insert_MauSac, id, giayChiTiet.getMauSac());
        connection.commit();
        return id;
    }

    @Override
    public List<GiayChiTiet> queryBySql(ResultSet rs) throws SQLException {
        List<GiayChiTiet> list = new ArrayList<>();
        while (rs.next()) {
            GiayChiTiet giayChiTiet = new GiayChiTiet();
            giayChiTiet.setIdGiayCT(rs.getInt("idGiayChiTiet"));
            Giay giay = new Giay();
            giay.setIdGiay(rs.getInt("idGiay"));
            giay.setTenGiay(rs.getString("tenGiay"));
            giay.setThuongHieu(rs.getString("thuongHieu"));
            giay.setNgayThem(rs.getTimestamp("ngayThem"));
            giay.setNgaySua(rs.getTimestamp("ngaySua"));
            giayChiTiet.setGiay(giay);
            giayChiTiet.setAnh(rs.getString("anh"));
            giayChiTiet.setMauSac(getMauSacByGiay(giayChiTiet.getIdGiayCT()));
            giayChiTiet.setGiaGoc(rs.getLong("giaGoc"));
            giayChiTiet.setSoLuong(rs.getInt("soLuong"));
            giayChiTiet.setKichCo(rs.getInt("kichCo"));
            giayChiTiet.setDeGiay(rs.getString("deGiay"));
            giayChiTiet.setNgayThem(rs.getTimestamp("ngayThem"));
            giayChiTiet.setNgaySua(rs.getTimestamp("ngaySua"));
            giayChiTiet.setTrangThai(rs.getBoolean("trangThai"));
            list.add(giayChiTiet);
        }
        return list;
    }

    @Override
    public List<GiayChiTiet> selectByKeyword(String keyword) throws SQLException {
        String sql = "select * from GiayChiTiet gct \n" +
                "join Giay g on g.idGiay = gct.idGiay \n" +
                "where gct.idGiayChiTiet like ? or g.tenGiay like ?";
        Connection connection = JdbcHelper.getConnection();
        PreparedStatement stmt = connection.prepareStatement(sql);
        JdbcHelper.setPrepareStatement(stmt, "%" + keyword + "%", "%" + keyword + "%");
        return queryBySql(stmt.executeQuery());
    }

    public void themMauSac(String sql, int idGiayChiTiet, List<String> mauSac) {
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = connection.prepareStatement(sql);
            for (String ms : mauSac) {
                stmt.setInt(1, idGiayChiTiet);
                stmt.setString(2, ms);
                stmt.executeUpdate();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public List<String> getMauSacByGiay(Integer id) {
        List<String> list = new ArrayList<>();
        String sql = "select tenMauSac from MauSac\n" +
                "where idGiayCT = ?";
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = connection.prepareStatement(sql);
            JdbcHelper.setPrepareStatement(stmt, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                String mauSac = rs.getString("tenMauSac");
                list.add(mauSac);
            }
            return list;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Integer deleteMauSac(String sql, Integer id) {
        Integer row = null;
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        return row;
    }

    public List<GiayChiTiet> findAllByTrangThai() {
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = connection.prepareStatement(select_all_sql_by_trang_thai);
            return queryBySql(stmt.executeQuery());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<GiayChiTiet> findRelative(int id) {
        try {
            Connection connection = JdbcHelper.getConnection();
            PreparedStatement stmt = connection.prepareStatement(select_relative);
            JdbcHelper.setPrepareStatement(stmt, id);
            return queryBySql(stmt.executeQuery());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
