package com.midori.repositories;

import com.midori.models.NhanVien;

import java.sql.SQLException;
import java.util.List;

public interface INhanVienRepositories extends ICrudRepositories<NhanVien, Integer> {
    public NhanVien findByUsername(String username) throws SQLException;
    public NhanVien findByEmail(String email) throws SQLException;
    public List<NhanVien> selectByKeyword(String keyword) throws SQLException;
}
