package com.midori.repositories;

import com.midori.models.GiayChiTiet;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface IGiayChiTietRepositories extends ICrudRepositories<GiayChiTiet, Integer> {
    public List<GiayChiTiet> selectByKeyword(String keyword) throws SQLException;
    public List<GiayChiTiet> findAllByTrangThai();
    public List<GiayChiTiet> findRelative(int id);
}
