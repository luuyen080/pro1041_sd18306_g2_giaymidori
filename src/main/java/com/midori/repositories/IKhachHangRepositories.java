package com.midori.repositories;

import com.midori.models.KhachHang;

import java.sql.SQLException;
import java.util.List;

public interface IKhachHangRepositories extends ICrudRepositories<KhachHang, Integer> {
    public List<KhachHang> selectByKeyword(String keyword) throws SQLException;
}
