package com.midori.repositories;

import com.midori.models.DonHang;
import com.midori.views.cart.CartModel;

import java.sql.SQLException;
import java.util.List;

public interface IDonHangRepositories extends ICrudRepositories<DonHang, Integer>{
    public List<CartModel> findDhctByIdDH(int id);
    public List<DonHang> selectByKeyword(String keyword) throws SQLException;
    public long findDailyRevenueForDate(int day, int month, int year);
    public long findDailyRevenueForMonth(int month, int year);

    public int findDailySanPhamForDate(int day, int month, int year);
    public int findDailySanPhamForMonth(int month, int year);
}
