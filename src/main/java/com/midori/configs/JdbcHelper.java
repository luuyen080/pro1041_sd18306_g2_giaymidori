/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midori.configs;

import com.midori.utils.Auth;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.Map;

public class JdbcHelper {
    private static Connection connection;

    static {
        try {
            Map<String, String> config = Auth.getConfig();
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String dbURL = "jdbc:sqlserver://" + config.get("server") + ":" + config.get("port") + ";databaseName=" + config.get("databaseName") + ";trustServerCertificate=true";
            String user = config.get("username");
            String pass = config.get("password");
            connection = DriverManager.getConnection(dbURL, user, pass);
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static Connection getConnection() throws SQLException {
        return connection;
    }


    public static void setPrepareStatement(PreparedStatement stmt, Object... args) throws SQLException {
        for (int i = 0; i < args.length; i++) {
            stmt.setObject(i + 1, args[i]);
        }
    }

    public static int getGeneratedKey (ResultSet rs) throws SQLException {
        int id = 0;
        while (rs.next()) {
            id = rs.getInt(1);
        }
        return id;
    }
}
