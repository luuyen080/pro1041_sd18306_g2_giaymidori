package com.midori.services;

import com.midori.models.NhanVien;

import java.sql.SQLException;
import java.util.List;

public interface INhanVienServices extends ICrudServices<NhanVien, Integer> {
    public NhanVien findByUsername(String username) throws SQLException;
    public NhanVien findByEmail(String email) throws SQLException;
    public List<NhanVien> selectByKeyword(String keyword) throws SQLException;
    public void updateUsernamePassword(NhanVien nhanVien);

}
