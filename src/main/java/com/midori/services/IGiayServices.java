package com.midori.services;

import com.midori.models.Giay;

import java.sql.SQLException;
import java.util.List;

public interface IGiayServices extends ICrudServices<Giay, Integer> {
    public List<Giay> selectByKeyword(String keyword) throws SQLException;
}
