package com.midori.services;

import com.midori.models.DonHang;

import java.sql.SQLException;
import java.util.List;

public interface IDonHangServices extends ICrudServices<DonHang, Integer> {
    public List<DonHang> selectByKeyword(String keyword) throws SQLException;

}
