package com.midori.services;

import java.sql.SQLException;
import java.util.List;

public interface ICrudServices<T, K> {
    public List<T> findAll();
    public T findById(K id);
    public String deleteById(K id) throws SQLException;
    public String update(T t) throws SQLException;
    public String save(T t) throws SQLException;
}
