package com.midori.services;

import com.midori.models.KhachHang;

import java.sql.SQLException;
import java.util.List;

public interface IKhachHangServices extends ICrudServices<KhachHang, Integer> {
    public List<KhachHang> selectByKeyword(String keyword) throws SQLException;
}
