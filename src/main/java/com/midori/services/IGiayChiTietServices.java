package com.midori.services;

import com.midori.models.GiayChiTiet;

import java.sql.SQLException;
import java.util.List;

public interface IGiayChiTietServices extends ICrudServices<GiayChiTiet, Integer> {
    public List<GiayChiTiet> selectByKeyword(String keyword) throws SQLException;
    public List<GiayChiTiet> findAllByTrangThai();
}
