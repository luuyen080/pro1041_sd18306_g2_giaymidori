package com.midori.services.Impl;

import com.midori.models.Giay;
import com.midori.repositories.Impl.GiayReporitories;
import com.midori.repositories.IGiayRepositories;
import com.midori.services.IGiayServices;

import java.sql.SQLException;
import java.util.List;

public class GiayServices implements IGiayServices {
    private IGiayRepositories giayRepositories;

    public GiayServices() {
        giayRepositories = new GiayReporitories();
    }

    @Override
    public List<Giay> findAll() {
        return giayRepositories.findAll();
    }

    @Override
    public Giay findById(Integer id)  {
        Giay giay = giayRepositories.findById(id);
        if (giay == null) {
            throw new IllegalStateException("Không tìm thấy giày");
        }
        return giay;
    }

    @Override
    public String deleteById(Integer id) throws SQLException {
        try {
            giayRepositories.deleteById(id);
        } catch (SQLException e) {
            return "Xoá thất bại: " + e.getMessage();
        }
        return "Xoá thành công";
    }

    @Override
    public String update(Giay giay) throws SQLException {
        giayRepositories.update(giay);
        return "Sửa thành công";
    }

    @Override
    public String save(Giay giay) throws SQLException {
        try {
            giayRepositories.save(giay);
        } catch (SQLException e) {
            return "Thêm thất bại: " + e.getMessage();
        }
        return "Thêm thành công";
    }

    public List<Giay> selectByKeyword(String keyword) throws SQLException {
        return giayRepositories.selectByKeyword(keyword);
    }
}
