package com.midori.services.Impl;

import com.midori.models.GiayChiTiet;
import com.midori.repositories.Impl.GiayChiTietRepositories;
import com.midori.repositories.IGiayChiTietRepositories;
import com.midori.services.IGiayChiTietServices;
import com.midori.utils.Img;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class GiayChiTietServices implements IGiayChiTietServices {
    private IGiayChiTietRepositories giayChiTietRepositories;

    public GiayChiTietServices() {
        giayChiTietRepositories = new GiayChiTietRepositories();
    }

    @Override
    public List<GiayChiTiet> findAll()  {
        return giayChiTietRepositories.findAll();
    }

    @Override
    public GiayChiTiet findById(Integer id)  {
        GiayChiTiet giayChiTiet = giayChiTietRepositories.findById(id);
        if (giayChiTiet == null) {
            throw new IllegalStateException("Không tìm thấy giày");
        }
        return giayChiTiet;
    }

    @Override
    public String deleteById(Integer id) throws SQLException {
        try {
            giayChiTietRepositories.deleteById(id);
        } catch (SQLException e) {
            boolean check = true;
            for (GiayChiTiet giayChiTiet : giayChiTietRepositories.findRelative(id)) {
                giayChiTiet.setTrangThai(false);
                giayChiTietRepositories.update(giayChiTiet);
                check = false;
            }
            if (!check) return "Không thể xóa giày do có thông tin liên quan đến các chức năng khác. Giày sẽ được chuyển sang trạng thái hết hàng!";
        }
        return "Xoá thành công";
    }

    @Override
    public String update(GiayChiTiet giayChiTiet) throws SQLException {
        if (giayChiTiet.getAnh() == null){
            giayChiTiet.setAnh("no-image.png");
        } else {
            File file = new File(giayChiTiet.getAnh());
            giayChiTiet.setAnh(file.getName());
            try {
                Img.saveImage(file);
            } catch (IOException e) {
                return e.getMessage();
            }
        }
        giayChiTietRepositories.update(giayChiTiet);
        return "Sửa thành công";
    }

    @Override
    public String save(GiayChiTiet giayChiTiet) throws SQLException {
        try {
            if (giayChiTiet.getAnh() == null) {
                giayChiTiet.setAnh("no-image.png");
            } else {
                File file = new File(giayChiTiet.getAnh());
                giayChiTiet.setAnh(file.getName());
                Img.saveImage(file);
                giayChiTietRepositories.save(giayChiTiet);
            }
        } catch (SQLException e) {
            return "Thêm thất bại: " + e.getMessage();
        } catch (IOException e) {
            return e.getMessage();
        }
        return "Thêm thành công";
    }

    public List<GiayChiTiet> selectByKeyword(String keyword) throws SQLException {
        return giayChiTietRepositories.selectByKeyword(keyword);
    }

    @Override
    public List<GiayChiTiet> findAllByTrangThai() {
        return giayChiTietRepositories.findAllByTrangThai();
    }
}
