package com.midori.services.Impl;

import com.midori.models.KhachHang;
import com.midori.repositories.IKhachHangRepositories;
import com.midori.repositories.Impl.KhachHangRepositories;
import com.midori.services.IKhachHangServices;

import java.sql.SQLException;
import java.util.List;

public class KhachHangServices implements IKhachHangServices {
    private IKhachHangRepositories khachHangRepositories;

    public KhachHangServices() {
        khachHangRepositories = new KhachHangRepositories();
    }

    @Override
    public List<KhachHang> findAll() {
        return khachHangRepositories.findAll();
    }

    @Override
    public KhachHang findById(Integer id)  {
        KhachHang khachHang = khachHangRepositories.findById(id);
        if (khachHang == null) {
            throw new IllegalStateException("Không tìm thấy khách hàng");
        }
        return khachHang;
    }

    @Override
    public String deleteById(Integer id) throws SQLException {
        try {
            khachHangRepositories.deleteById(id);
        } catch (SQLException e) {
            return "Xoá thất bại: " + e.getMessage();
        }
        return "Xoá thành công";
    }

    @Override
    public String update(KhachHang khachHang) throws SQLException {
        khachHangRepositories.update(khachHang);
        return "Sửa thành công";
    }

    @Override
    public String save(KhachHang khachHang) throws SQLException {
        try {
            khachHangRepositories.save(khachHang);
        } catch (SQLException e) {
            return "Thêm thất bại: " + e.getMessage();
        }
        return "Thêm thành công";
    }

    public List<KhachHang> selectByKeyword(String keyword) throws SQLException {
        return khachHangRepositories.selectByKeyword(keyword);
    }
}
