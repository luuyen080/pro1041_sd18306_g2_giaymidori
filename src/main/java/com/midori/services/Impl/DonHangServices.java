package com.midori.services.Impl;

import com.midori.models.DonHang;
import com.midori.models.GiayChiTiet;
import com.midori.models.KhachHang;
import com.midori.repositories.IDonHangRepositories;
import com.midori.repositories.IGiayChiTietRepositories;
import com.midori.repositories.IKhachHangRepositories;
import com.midori.repositories.INhanVienRepositories;
import com.midori.repositories.Impl.DonHangRepositories;
import com.midori.repositories.Impl.GiayChiTietRepositories;
import com.midori.repositories.Impl.KhachHangRepositories;
import com.midori.repositories.Impl.NhanVienRepositories;
import com.midori.services.IDonHangServices;
import com.midori.views.cart.CartModel;
import com.midori.views.cart.CustomSpinner;

import java.sql.SQLException;
import java.util.List;

public class DonHangServices implements IDonHangServices {

    private IDonHangRepositories donHangRepositories;
    private IKhachHangRepositories khachHangRepositories;
    private INhanVienRepositories nhanVienRepositories;
    private IGiayChiTietRepositories giayChiTietRepositories;

    public DonHangServices() {
        donHangRepositories = new DonHangRepositories();
        khachHangRepositories = new KhachHangRepositories();
        nhanVienRepositories = new NhanVienRepositories();
        giayChiTietRepositories = new GiayChiTietRepositories();
    }

    @Override
    public List<DonHang> findAll() {
        List<DonHang> list = donHangRepositories.findAll();
        for (DonHang donHang : list) {
            donHang.setNv(nhanVienRepositories.findById(donHang.getNv().getId()));
            donHang.setKh(khachHangRepositories.findById(donHang.getKh().getIdKhachHang()));
            List<CartModel> listGH = donHangRepositories.findDhctByIdDH(donHang.getId());
            for (CartModel cartModel : listGH) {
                cartModel.setGiayChiTiet(giayChiTietRepositories.findById(cartModel.getGiayChiTiet().getIdGiayCT()));
            }
            donHang.setGioHangModels(listGH);
        }
        return list;
    }

    @Override
    public DonHang findById(Integer id) {
        DonHang donHang = donHangRepositories.findById(id);
        if (donHang == null) {
            throw new IllegalStateException("Không tìm thấy đơn hàng");
        }
        donHang.setNv(nhanVienRepositories.findById(donHang.getNv().getId()));
        donHang.setKh(khachHangRepositories.findById(donHang.getKh().getIdKhachHang()));
        List<CartModel> listGH = donHangRepositories.findDhctByIdDH(donHang.getId());
        for (CartModel cartModel : listGH) {
            cartModel.setGiayChiTiet(giayChiTietRepositories.findById(cartModel.getGiayChiTiet().getIdGiayCT()));
        }
        donHang.setGioHangModels(listGH);
        return donHang;
    }

    @Override
    public String deleteById(Integer id) throws SQLException {
        try {
            nhanVienRepositories.deleteById(id);
        } catch (SQLException e) {
            return "Xoá thất bại: " + e.getMessage();
        }
        return "Xoá thành công";
    }

    @Override
    public String update(DonHang donHang) throws SQLException {
        donHangRepositories.update(donHang);
        return "Sửa thành công";
    }

    @Override
    public String save(DonHang donHang) throws SQLException {
        List<CartModel> chiTietDonHang = donHang.getGioHangModels();
        for (int i = 0; i < chiTietDonHang.size(); i++) {
            CartModel chiTietDonHang1 = chiTietDonHang.get(i);
            GiayChiTiet giayChiTiet = giayChiTietRepositories.findById(chiTietDonHang1.getGiayChiTiet().getIdGiayCT());
            if ((int) chiTietDonHang1.getSoLuong().getValue() <= giayChiTiet.getSoLuong()) {
                giayChiTiet.setSoLuong(giayChiTiet.getSoLuong() - (int) chiTietDonHang1.getSoLuong().getValue());
                if (giayChiTiet.getSoLuong() <= 0) {
                    giayChiTiet.setTrangThai(false);
                }
                giayChiTietRepositories.update(giayChiTiet);
            } else throw new IllegalStateException("Số lượng sản phẩm vượt quá số lượng còn lại");
        }
        donHangRepositories.save(donHang);

//        if (donHang.getTongTien() >= 500000) {
//            KhachHang khachHang = khachHangRepositories.findById(donHang.getKh().getIdKhachHang());
//            int point = khachHang.getDiemTichLuy();
//            khachHang.setDiemTichLuy(point + 50);
//            khachHangRepositories.update(khachHang);
//        }

        return "Thêm thành công";
    }

    @Override
    public List<DonHang> selectByKeyword(String keyword) throws SQLException {
        return donHangRepositories.selectByKeyword(keyword);
    }
}
