package com.midori.services.Impl;

import com.midori.models.DoanhThuNam;
import com.midori.models.DoanhThuNgay;
import com.midori.models.SanPhamNam;
import com.midori.models.SanPhamNgay;
import com.midori.repositories.IDonHangRepositories;
import com.midori.repositories.Impl.DonHangRepositories;
import com.midori.services.IThongKeServices;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ThongKeServices implements IThongKeServices {
    IDonHangRepositories repository = new DonHangRepositories();
    @Override
    public List<DoanhThuNgay> getAllTongTienTheoThang(int month, int year) {
        List<DoanhThuNgay> result = new ArrayList<>();
        LocalDateTime startDate = LocalDateTime.of(year, month, 1, 0, 0);
        LocalDateTime endDate = startDate.plusMonths(1).minusDays(1);
        while (!startDate.isAfter(endDate)) {
            int day = startDate.getDayOfMonth();
            DoanhThuNgay dto = new DoanhThuNgay(repository.findDailyRevenueForDate(day, month, year), day);
            dto.setTongTien(Optional.ofNullable(dto.getTongTien()).orElse(0L));
            result.add(dto);
            startDate = startDate.plusDays(1);
        }
        return result;
    }
    @Override
    public List<DoanhThuNam> getAllTongTienTheoNam(int year) {
        List<DoanhThuNam> result = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            Long revenue = repository.findDailyRevenueForMonth(i, year);
            DoanhThuNam doanhThuNam = new DoanhThuNam(revenue, i);
            doanhThuNam.setTongTien(Optional.ofNullable(doanhThuNam.getTongTien()).orElse(0L));
            result.add(doanhThuNam);
        }
        return result;
    }

    @Override
    public Integer countDonHangByNhanVien(Integer id) {
        return null;
    }

    @Override
    public Integer countAllDonhang() {
        return null;
    }

    @Override
    public List<SanPhamNgay> getAllSanPhamTheoThang(int month, int year) {
        List<SanPhamNgay> result = new ArrayList<>();
        LocalDateTime startDate = LocalDateTime.of(year, month, 1, 0, 0);
        LocalDateTime endDate = startDate.plusMonths(1).minusDays(1);
        while (!startDate.isAfter(endDate)) {
            int day = startDate.getDayOfMonth();
            SanPhamNgay sanPhamNgay = new SanPhamNgay(repository.findDailySanPhamForDate(day, month, year), day);
            sanPhamNgay.setSoLuong(Optional.ofNullable(sanPhamNgay.getSoLuong()).orElse(0));
            result.add(sanPhamNgay);
            startDate = startDate.plusDays(1);
        }
        return result;
    }

    @Override
    public List<SanPhamNam> getAllSanPhamTheoNam(int year) {
        List<SanPhamNam> result = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            int revenue = repository.findDailySanPhamForMonth(i, year);
            SanPhamNam sanPhamNam = new SanPhamNam(revenue, i);
            sanPhamNam.setSoLuong(Optional.ofNullable(sanPhamNam.getSoLuong()).orElse(0));
            result.add(sanPhamNam);
        }
        return result;
    }
}
