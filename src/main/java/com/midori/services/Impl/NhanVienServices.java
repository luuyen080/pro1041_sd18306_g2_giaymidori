package com.midori.services.Impl;

import com.midori.models.NhanVien;
import com.midori.repositories.INhanVienRepositories;
import com.midori.repositories.Impl.NhanVienRepositories;
import com.midori.services.INhanVienServices;
import com.midori.utils.DTOUtils;
import com.midori.utils.HashUtils;
import com.midori.utils.Img;
import com.midori.utils.MsgBox;
import org.apache.commons.validator.Msg;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class NhanVienServices implements INhanVienServices {
    private INhanVienRepositories nhanVienRepositories;

    public NhanVienServices() {
        nhanVienRepositories = new NhanVienRepositories();
    }

    @Override
    public List<NhanVien> findAll() {
        return nhanVienRepositories.findAll();
    }

    @Override
    public NhanVien findById(Integer id)  {
        NhanVien nhanVien = nhanVienRepositories.findById(id);
        if (nhanVien == null) {
            throw new IllegalStateException("Không tìm thấy tài khoản nhân viên!");
        }
        return nhanVien;
    }

    @Override
    public String deleteById(Integer id) throws SQLException {
        try {
            nhanVienRepositories.deleteById(id);
        } catch (SQLException e) {
            return "Xoá thất bại: " + e.getMessage();
        }
        return "Xoá thành công";

    }

    @Override
    public String update(NhanVien nhanVien) throws SQLException {
        try {
            if (nhanVien.getAnh() == null){
                nhanVien.setAnh("no-image.png");
            } else {
                File file = new File(nhanVien.getAnh());
                nhanVien.setAnh(file.getName());
                try {
                    Img.saveImage(file);
                } catch (IOException e) {
                    return e.getMessage();
                }
            }
            nhanVienRepositories.update(nhanVien);
        } catch (SQLException e) {
            return "Username đã tồn tại!";
        }
        return "Sửa thành công";
    }

    @Override
    public String save(NhanVien nhanVien) throws SQLException {
        try {
            if (nhanVien.getAnh() == null) {
                nhanVien.setAnh("no-image.png");
            } else {
                File file = new File(nhanVien.getAnh());
                nhanVien.setAnh(file.getName());
                Img.saveImage(file);
                nhanVienRepositories.save(nhanVien);
            }
        } catch (SQLException e) {
            return "Thêm thất bại: " + e.getMessage();
        } catch (IOException e) {
            return e.getMessage();
        }
        return "Thêm thành công";
    }

    @Override
    public NhanVien findByUsername(String username) throws SQLException {
        NhanVien nhanVien = nhanVienRepositories.findByUsername(username);
        if (nhanVien == null) {
            throw new IllegalStateException("Không tìm thấy tài khoản nhân viên!");
        }
        return nhanVien;
    }

    @Override
    public NhanVien findByEmail(String email) throws SQLException {
        NhanVien nhanVien = nhanVienRepositories.findByEmail(email);
        if (nhanVien == null) {
            throw new IllegalStateException("Không tìm thấy email nhân viên!");
        }
        return nhanVien;
    }

    public List<NhanVien> selectByKeyword(String keyword) throws SQLException {
        return nhanVienRepositories.selectByKeyword(keyword);
    }

    @Override
    public void updateUsernamePassword(NhanVien nhanVien) {
        NhanVien nhanVien1 = nhanVienRepositories.findById(nhanVien.getId());
        nhanVien1.setPassword(nhanVien.getPassword());
        nhanVien.setUsername(nhanVien1.getUsername());
        try {
            nhanVien1.setId(nhanVien.getId());
            nhanVienRepositories.update(nhanVien1);
        } catch (SQLException e) {
            MsgBox.alert(null, e.getMessage());
        }
    }
}
