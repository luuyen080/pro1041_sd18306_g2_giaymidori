package com.midori.services;

import com.midori.models.DoanhThuNam;
import com.midori.models.DoanhThuNgay;
import com.midori.models.SanPhamNam;
import com.midori.models.SanPhamNgay;

import java.util.List;

public interface IThongKeServices {
    List<DoanhThuNgay>  getAllTongTienTheoThang(int month, int year);
    List<DoanhThuNam> getAllTongTienTheoNam(int year);
    Integer countDonHangByNhanVien(Integer id);
    Integer countAllDonhang();
    List<SanPhamNgay> getAllSanPhamTheoThang(int month, int year);
    List<SanPhamNam> getAllSanPhamTheoNam(int year);
}
