package com.midori.utils;

import com.midori.models.NhanVien;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;

import javax.swing.*;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Auth {
    private static final String CONFIG_FILE_PATH = "config.yaml";
    public static String otp;
    public static JPanel content;

//    khai báo user => Duy trì user đăng nhập vào hệ thống
    public static NhanVien user = null;

//    Xóa user khi người dùng đăng xuất
    public static void clear() {
        Auth.user = null;
    }
    public static boolean isLogin() {
//        user khác null => Đã đăng nhập
        return Auth.user != null;
    }
    public static boolean isManager() {
//        Kiểm tra vai trò trưởng phòng hay nhân viên
        return Auth.isLogin() && user.isVaiTro();
    }

    public static boolean isValidEmail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static Map<String, String> getConfig() {
        Map<String, String> data = null;
        try {
            FileInputStream input = new FileInputStream(CONFIG_FILE_PATH);
            Yaml yaml = new Yaml();
            data = yaml.load(input);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            MsgBox.alert(null, "Lỗi đọc file config.yaml");
            System.exit(0);
        }
        return data;
    }

    public static boolean createDefaultConfigFile() {
        File configFile = new File(CONFIG_FILE_PATH);
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
                writeDefaultConfig();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public static String isConfigFileCreated() {
        File configFile = new File(CONFIG_FILE_PATH);
        return configFile.getAbsolutePath();
    }

    private static void writeDefaultConfig() throws IOException {
        Map<String, Object> defaultConfig = new HashMap<>();
        defaultConfig.put("server", "'localhost'");
        defaultConfig.put("password", "''");
        defaultConfig.put("databaseName", "''");
        defaultConfig.put("port", "''1433''");
        defaultConfig.put("username", "''sa''");

        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setAllowReadOnlyProperties(true);

        Representer representer = new Representer();
        representer.addClassTag(defaultConfig.getClass(), Tag.MAP);

        Yaml yaml = new Yaml(representer, options);
        try (FileWriter writer = new FileWriter(CONFIG_FILE_PATH)) {
            yaml.dump(defaultConfig, writer);
        }
    }
}
