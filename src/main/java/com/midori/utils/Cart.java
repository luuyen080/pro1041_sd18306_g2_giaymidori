package com.midori.utils;

import com.jgoodies.common.collect.ArrayListModel;
import com.midori.Application;
import com.midori.views.cart.CartModel;

import javax.swing.*;
import java.text.DecimalFormat;
import java.util.List;

public class Cart {
    private static List<CartModel> list;

    static {
        list = new ArrayListModel<>();
    }

    public static List<CartModel> getCart() {
        return list;
    }

    public static void tinhTien(JTable table, JLabel lblTT) {
        if (table.getRowCount() == 0) {
            lblTT.setText("0đ");
        } else {
            long tong = 0;
            for (int i = 0; i < table.getRowCount(); i++) {
                long donGia = CurrencyConverter.parseLong(table.getValueAt(i, 1).toString());
                int soLuong = Integer.parseInt(((JSpinner) table.getValueAt(i, 2)).getValue().toString());
                tong += donGia * soLuong;
            }
            lblTT.setText(CurrencyConverter.parseString(tong));
        }
        Application.app.repaint();
    }
}
