package com.midori.models;

import lombok.Data;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;

@Data
public class NhanVien {
    private int id;
    private String username;
    private String password;
    private boolean vaiTro;
    private String tenNhanVien;
    private String anh;
    private String sdt;
    private String email;
    private Timestamp ngayTao;
    private boolean trangThai;

    public NhanVien() {
    }

    public NhanVien(String username, String password, boolean vaiTro, String tenNhanVien, String anh, String sdt, String email, boolean trangThai) {
        this.username = username;
        this.password = password;
        this.vaiTro = vaiTro;
        this.tenNhanVien = tenNhanVien;
        this.anh = anh;
        this.sdt = sdt;
        this.email = email;
        this.trangThai = trangThai;
    }

    public NhanVien(String username, String password, boolean vaiTro, String tenNhanVien, String anh, String sdt, String email, Timestamp ngayTao, boolean trangThai) {
        this.username = username;
        this.password = password;
        this.vaiTro = vaiTro;
        this.tenNhanVien = tenNhanVien;
        this.anh = anh;
        this.sdt = sdt;
        this.email = email;
        this.ngayTao = ngayTao;
        this.trangThai = trangThai;
    }

    public Object[] toObject() {
        return new Object[]{
                id,
                username,
                tenNhanVien,
                anh,
                vaiTro ? "Quản lý" : "Nhân viên",
                sdt,
                email,
                (ngayTao != null) ? ngayTao.toLocalDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " " + ngayTao.toLocalDateTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")) : ngayTao,
                trangThai ? "Đang làm việc" : "Đã nghỉ việc"};
    }
}
