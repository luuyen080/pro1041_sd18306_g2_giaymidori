package com.midori.models;

import lombok.Data;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

@Data
public class Giay {
    private int idGiay;
    private String tenGiay;
    private String thuongHieu;
    private Timestamp ngayThem;
    private Timestamp ngaySua;

    public Giay() {
    }

    public Giay(int idGiay, String tenGiay, String thuongHieu, Timestamp ngayThem, Timestamp ngaySua) {
        this.idGiay = idGiay;
        this.tenGiay = tenGiay;
        this.thuongHieu = thuongHieu;
        this.ngayThem = ngayThem;
        this.ngaySua = ngaySua;
    }

    public Giay(String tenGiay, String thuongHieu, Timestamp ngayThem, Timestamp ngaySua) {
        this.tenGiay = tenGiay;
        this.thuongHieu = thuongHieu;
        this.ngayThem = ngayThem;
        this.ngaySua = ngaySua;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Giay giay = (Giay) o;
        return giay.getTenGiay().equals(tenGiay);
    }

    @Override
    public String toString() {
        return tenGiay;
    }
    public Object[] toObject() {
        return new Object[]{
                idGiay,
                tenGiay,
                thuongHieu,
                (ngayThem != null) ? ngayThem.toLocalDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " " + ngayThem.toLocalDateTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")) : ngayThem,
                (ngaySua != null) ? ngaySua.toLocalDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " " + ngaySua.toLocalDateTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")) : ngaySua
        };
    }
}
