package com.midori.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class DoanhThuNgay {
    private long tongTien;
    private int ngay;

    public DoanhThuNgay() {
    }

    public DoanhThuNgay(long tongTien, int ngay) {
        this.tongTien = tongTien;
        this.ngay = ngay;
    }
}
