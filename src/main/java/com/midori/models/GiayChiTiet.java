package com.midori.models;

import com.midori.utils.CurrencyConverter;
import lombok.Data;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Data
public class GiayChiTiet {
    private int idGiayCT;
    private Giay giay;
    private String anh;
    private List<String> mauSac;
    private long giaGoc;
    private int soLuong;
    private int kichCo;
    private String deGiay;
    private Timestamp ngayThem;
    private Timestamp ngaySua;
    private boolean trangThai;

    public GiayChiTiet() {
    }

    public GiayChiTiet(int idGiayCT, Giay giay, String anh, List<String> mauSac, long giaGoc, int soLuong, int kichCo, String deGiay, Timestamp ngayThem, Timestamp ngaySua, boolean trangThai) {
        this.idGiayCT = idGiayCT;
        this.giay = giay;
        this.anh = anh;
        this.mauSac = mauSac;
        this.giaGoc = giaGoc;
        this.soLuong = soLuong;
        this.kichCo = kichCo;
        this.deGiay = deGiay;
        this.ngayThem = ngayThem;
        this.ngaySua = ngaySua;
        this.trangThai = trangThai;
    }

    public GiayChiTiet(Giay giay, String anh, List<String> mauSac, long giaGoc, int soLuong, int kichCo, String deGiay, Timestamp ngayThem, Timestamp ngaySua, boolean trangThai) {
        this.giay = giay;
        this.anh = anh;
        this.mauSac = mauSac;
        this.giaGoc = giaGoc;
        this.soLuong = soLuong;
        this.kichCo = kichCo;
        this.deGiay = deGiay;
        this.ngayThem = ngayThem;
        this.ngaySua = ngaySua;
        this.trangThai = trangThai;
    }

    public Object[] toObject() {
        return new Object[]{
                idGiayCT,
                giay.getTenGiay(),
                anh,
                mauSac,
                CurrencyConverter.parseString(giaGoc),
                soLuong,
                kichCo,
                deGiay,
                (ngayThem != null) ? ngayThem.toLocalDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " " + ngayThem.toLocalDateTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")) : ngayThem,
                (ngaySua != null) ? ngaySua.toLocalDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " " + ngaySua.toLocalDateTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")) : ngaySua,
                trangThai ? "Còn hàng" : "Hết hàng"
        };
    }
}
