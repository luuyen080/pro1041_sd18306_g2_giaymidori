package com.midori.models;

import lombok.Data;

@Data
public class SanPhamNgay {
    private int soLuong;
    private int ngay;

    public SanPhamNgay() {
    }

    public SanPhamNgay(int soLuong, int ngay) {
        this.soLuong = soLuong;
        this.ngay = ngay;
    }
}
