package com.midori.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class DoanhThuNam {
    private long tongTien;
    private int thang;

    public DoanhThuNam() {
    }

    public DoanhThuNam(long tongTien, int thang) {
        this.tongTien = tongTien;
        this.thang = thang;
    }
}
