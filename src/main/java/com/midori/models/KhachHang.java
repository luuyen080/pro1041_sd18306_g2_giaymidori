package com.midori.models;

import lombok.Data;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;

@Data
public class KhachHang {
    private int idKhachHang;
    private String tenKhachHang;
    private String sdt;
    private int diemTichLuy;
    private Timestamp ngayThem;
    private boolean gioiTinh;

    public KhachHang() {
    }

    public KhachHang(int idKhachHang, String tenKhachHang, String sdt, int diemTichLuy, Timestamp ngayThem, boolean gioiTinh) {
        this.idKhachHang = idKhachHang;
        this.tenKhachHang = tenKhachHang;
        this.sdt = sdt;
        this.diemTichLuy = diemTichLuy;
        this.ngayThem = ngayThem;
        this.gioiTinh = gioiTinh;
    }

    public KhachHang(String tenKhachHang, String sdt, int diemTichLuy, Timestamp ngayThem, boolean gioiTinh) {
        this.tenKhachHang = tenKhachHang;
        this.sdt = sdt;
        this.diemTichLuy = diemTichLuy;
        this.ngayThem = ngayThem;
        this.gioiTinh = gioiTinh;
    }

    @Override
    public String toString() {
        return idKhachHang + " - " + tenKhachHang;
    }

    public Object[] toObject() {
        return new Object[]{
                idKhachHang,
                tenKhachHang,
                gioiTinh ? "Nam" : "Nữ",
                sdt,
                diemTichLuy,
                (ngayThem != null) ? ngayThem.toLocalDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " " + ngayThem.toLocalDateTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")) : ngayThem
        };
    }
}
