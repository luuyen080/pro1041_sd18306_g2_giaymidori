package com.midori.models;

import lombok.Data;

@Data
public class SanPhamNam {
    private int soLuong;
    private int thang;

    public SanPhamNam() {
    }

    public SanPhamNam(int soLuong, int thang) {
        this.soLuong = soLuong;
        this.thang = thang;
    }
}
