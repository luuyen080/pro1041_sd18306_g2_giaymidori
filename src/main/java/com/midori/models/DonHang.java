package com.midori.models;

import com.midori.utils.CurrencyConverter;
import com.midori.views.cart.CartModel;
import lombok.Data;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Data
public class DonHang {
    private int id;
    private KhachHang kh;
    private NhanVien nv;
    private long tongTien;
    private Timestamp ngayThem;
    private List<CartModel> gioHangModels;

    public DonHang() {
    }

    public DonHang(int id, KhachHang kh, NhanVien nv, long tongTien, Timestamp ngayThem, List<CartModel> gioHangModels) {
        this.id = id;
        this.kh = kh;
        this.nv = nv;
        this.tongTien = tongTien;
        this.ngayThem = ngayThem;
        this.gioHangModels = gioHangModels;
    }

    public DonHang(KhachHang kh, NhanVien nv, long tongTien, Timestamp ngayThem, List<CartModel> gioHangModels) {
        this.kh = kh;
        this.nv = nv;
        this.tongTien = tongTien;
        this.ngayThem = ngayThem;
        this.gioHangModels = gioHangModels;
    }

    public Object[] toObject()
    {
        return new Object[] {
                id,
                nv.getId(),
                kh.getTenKhachHang(),
                CurrencyConverter.parseString(tongTien),
                (ngayThem != null) ? ngayThem.toLocalDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " " + ngayThem.toLocalDateTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")) : ngayThem
        };
    }
}
