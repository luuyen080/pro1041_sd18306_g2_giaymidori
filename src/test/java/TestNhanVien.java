import com.midori.models.NhanVien;
import com.midori.repositories.IKhachHangRepositories;
import com.midori.repositories.INhanVienRepositories;
import com.midori.repositories.Impl.KhachHangRepositories;
import com.midori.repositories.Impl.NhanVienRepositories;
import com.midori.services.INhanVienServices;
import com.midori.services.Impl.NhanVienServices;
import org.junit.Test;

import java.sql.SQLException;

public class TestNhanVien {
    @org.junit.Test
    public void testFindAllNV() {
        INhanVienRepositories nhanVienRepositories = new NhanVienRepositories();
        for (NhanVien nhanVien : nhanVienRepositories.findAll()) {
            System.out.println(nhanVien.toString());
        }
    }

    @Test
    public void testFindByUsername() {
        INhanVienServices nhanVienServices = new NhanVienServices();
        try {
            System.out.println(nhanVienServices.findByUsername("NV001"));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @org.junit.Test
    public void testThemNhanVien() {
        INhanVienRepositories nhanVienRepositories = new NhanVienRepositories();
        NhanVien nhanVien = new NhanVien("yen", "123", true, "yen", null, null, "yen@gmail.com", true);
        try {
            System.out.println(nhanVienRepositories.save(nhanVien));;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @org.junit.Test
    public void testSuaNhanVien() {
        INhanVienRepositories nhanVienRepositories = new NhanVienRepositories();
        NhanVien nhanVien = new NhanVien("yen", "234", true, "yen", null, null, "yen@gmail.com", true);
        try {
            nhanVien.setId(8);
            nhanVienRepositories.update(nhanVien);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @org.junit.Test
    public void testFindByIdNV() {
        // Nhap id = -1 co hien thong bao khong tim thay
        INhanVienServices nhanVienServices = new NhanVienServices();
        nhanVienServices.findById(-1);
    }

    @org.junit.Test
    public void testFindByID() {
        // Nhap id = 1 co tra ra doi tuong co id 1 khong
//        INhanVienServices nhanVienServices = new NhanVienServices();
//        System.out.println(nhanVienServices.findById(1));
        IKhachHangRepositories khachHangRepositories = new KhachHangRepositories();
        System.out.println(khachHangRepositories.findById(1));
    }

    @Test
    public void testTimKiem() {
        INhanVienRepositories nhanVienRepositories = new NhanVienRepositories();
        try {
            System.out.println(nhanVienRepositories.selectByKeyword("1"));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        ;
    }
}
