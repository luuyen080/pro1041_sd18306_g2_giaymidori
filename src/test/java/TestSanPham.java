import com.midori.models.Giay;
import com.midori.models.GiayChiTiet;
import com.midori.models.KhachHang;
import com.midori.repositories.*;
import com.midori.repositories.IKhachHangRepositories;
import com.midori.repositories.Impl.GiayChiTietRepositories;
import com.midori.repositories.Impl.KhachHangRepositories;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

public class TestSanPham {
    @Test
    public void testFindAllGiayChiTiet() {
        IGiayChiTietRepositories giayChiTietRepositories = new GiayChiTietRepositories();
        for (GiayChiTiet giayChiTiet : giayChiTietRepositories.findAll()) {
            System.out.println(giayChiTiet.toString());
        }
    }

    @org.junit.Test
    public void testThemSanPham() throws SQLException {
        IGiayChiTietRepositories giayChiTietRepositories = new GiayChiTietRepositories();
        GiayChiTiet giayChiTiet = new GiayChiTiet(
                new Giay(1, "Giày Air Jordan 1 x Off-White Retro High OG", "ADIDAS", null, null),
                null,
                List.of("Vàng", "Trắng"),
                300000,
                10,
                38,
                "Đế giày da",
                null,
                null,
                true);
        giayChiTietRepositories.save(giayChiTiet);
    }

    @org.junit.Test
    public void testSuaSanPham() {
        IGiayChiTietRepositories giayChiTietRepositories = new GiayChiTietRepositories();
        GiayChiTiet giayChiTiet = new GiayChiTiet(
                new Giay(1, "Giày Air Jordan 1 x Off-White Retro High OG", "ADIDAS", null, null),
                null,
                List.of("Vàng", "Trắng"),
                300000,
                5,
                38,
                "Đế giày da",
                null,
                null,
                true);
        try {
            giayChiTiet.setIdGiayCT(56);
            giayChiTietRepositories.update(giayChiTiet);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testXoaSanPham() {
        IGiayChiTietRepositories giayChiTietRepositories = new GiayChiTietRepositories();
        try {
            giayChiTietRepositories.deleteById(55);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @org.junit.Test
    public void testFindById() {
        IGiayChiTietRepositories giayChiTietRepositories = new GiayChiTietRepositories();
        System.out.println(giayChiTietRepositories.findById(50));
    }

    @Test
    public void testFindRelative() {
        IGiayChiTietRepositories giayChiTietRepositories = new GiayChiTietRepositories();
        for (GiayChiTiet giayChiTiet : giayChiTietRepositories.findRelative(10)) {
            System.out.println(giayChiTiet.toString());
        }
    }
}
