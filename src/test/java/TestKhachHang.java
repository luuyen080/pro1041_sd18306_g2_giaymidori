import com.midori.models.KhachHang;
import com.midori.repositories.IKhachHangRepositories;
import com.midori.repositories.Impl.KhachHangRepositories;

import java.sql.SQLException;

public class TestKhachHang {
    @org.junit.Test
    public void testFindAllKH() {
        IKhachHangRepositories khachHangRepositories = new KhachHangRepositories();
        for (KhachHang khachHang : khachHangRepositories.findAll()) {
            System.out.println(khachHang.toString());
        }
    }

    @org.junit.Test
    public void testThemKh() {
        IKhachHangRepositories khachHangRepositories = new KhachHangRepositories();
        KhachHang khachHang = new KhachHang("Khanh", "123", 30, null, false);
        try {
            khachHangRepositories.save(khachHang);
        } catch (SQLException throwables) {
            throw new RuntimeException(throwables);
        }
        for (KhachHang khachHang1 : khachHangRepositories.findAll()) {
            System.out.println(khachHang1.toString());
        }
    }

    @org.junit.Test
    public void testSuaKH() {
        IKhachHangRepositories khachHangRepositories = new KhachHangRepositories();
        KhachHang khachHang = new KhachHang("Khanh", "123123", 30, null, false);
        khachHang.setIdKhachHang(8);
        try {
            khachHangRepositories.update(khachHang);
            for (KhachHang khachHang1 : khachHangRepositories.findAll()) {
                System.out.println(khachHang1.toString());
            }
        } catch (com.microsoft.sqlserver.jdbc.SQLServerException throwables) {
            throw new RuntimeException(throwables);
        } catch (SQLException throwables) {
            throw new RuntimeException(throwables);
        }
    }
}
