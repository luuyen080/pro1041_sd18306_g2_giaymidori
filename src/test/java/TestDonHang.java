import com.midori.models.DonHang;
import com.midori.repositories.IDonHangRepositories;
import com.midori.repositories.Impl.DonHangRepositories;
import com.midori.services.IDonHangServices;
import com.midori.services.Impl.DonHangServices;
import org.junit.Test;

import java.sql.SQLException;

public class TestDonHang {
    @Test
    public void getAllDH() {
        IDonHangServices donHangServices = new DonHangServices();
        System.out.println(donHangServices.findAll());
    }

    @Test
    public void testGH() {
        IDonHangRepositories donHangRepositories = new DonHangRepositories();
        System.out.println(donHangRepositories.findDhctByIdDH(6));
    }

    @Test
    public void test() {
        IDonHangRepositories donHangRepositories = new DonHangRepositories();
        System.out.println(donHangRepositories.findDhctByIdDH(1));;
    }

    @Test
    public void testTimKiem() {
        IDonHangServices donHangServices = new DonHangServices();
        try {
            for (DonHang donHang : donHangServices.selectByKeyword("2")) {
                System.out.println(donHang);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testFindDailyRevenueForDate() {
        IDonHangRepositories donHangRepositories = new DonHangRepositories();
        System.out.println(donHangRepositories.findDailyRevenueForDate(8, 12, 2023));
    }

    @Test
    public void testFindDailyRevenueForMonth() {
        IDonHangRepositories donHangRepositories = new DonHangRepositories();
        System.out.println(donHangRepositories.findDailyRevenueForMonth(12, 2024));
    }

    @Test
    public void testFindSanPhamForDate() {
        IDonHangRepositories donHangRepositories = new DonHangRepositories();
        System.out.println(donHangRepositories.findDailySanPhamForDate(9, 12, 2023));
    }

    @Test
    public void testFindSanPhamForMonth() {
        IDonHangRepositories donHangRepositories = new DonHangRepositories();
        System.out.println(donHangRepositories.findDailySanPhamForMonth(12, 2023));
    }
}
